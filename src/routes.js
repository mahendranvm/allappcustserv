angular
  .module('csapp')
  .config(routesConfig);

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/cs/publish/reqtopub',{},{reload:true});

  $stateProvider
  .state('login', {
        url: '/login?email&msg',
        templateUrl: 'app/_auth/login.html',
        controller: 'loginctrl'
    })
    .state('forgotpwd', {
        url: '/forgotpwd?email&msg',
        templateUrl: 'app/_auth/forgotpwd.html',
        controller: 'forgotpwdctrl'
    })
    .state('resetpwd', {
        url: '/resetpwd?email',
        templateUrl: 'app/_auth/resetpwd.html',
        controller: 'resetpwdctrl'
    })
    .state('register', {
        url: '/register?email&msg',
        templateUrl: 'app/_auth/register.html',
        controller: 'registerctrl'
    })
    .state('verify', {
        url: '/verify?email&msg',
        templateUrl: 'app/_auth/verify.html',
        controller: 'verifyctrl'
    })
    .state('newcode', {
        url: '/newcode?email&msg',
        templateUrl: 'app/_auth/newcode.html',
        controller: 'newcodectrl'
    })
    .state('cs', {
        abstract:true,
        url: '/cs',
        templateUrl: 'app/cs.html',
        controller: 'csctrl'
    })
    .state('cs.publish', {
        abstract:true,
        url: '/publish',
        templateUrl: 'app/_cs/publish.html',
        controller: 'publishctrl'
    })
    .state('cs.publish.notpublished', {
        url: '/notpublished',
        templateUrl: 'app/_cs/_publish/notpublished.html',
        controller: 'notpublishedctrl'
    })
    .state('cs.publish.reqtopub', {
        url: '/reqtopub',
        templateUrl: 'app/_cs/_publish/reqtopub.html',
        controller: 'reqtopubctrl'
    })
    .state('cs.publish.package', {
        url: '/package',
        templateUrl: 'app/_cs/_publish/package.html',
        controller: 'packagectrl'
    })
    .state('cs.publish.toappstores', {
        url: '/toappstores',
        templateUrl: 'app/_cs/_publish/toappstores.html',
        controller: 'toappstoresctrl'
    })
    .state('cs.publish.partpub', {
        url: '/partpub',
        templateUrl: 'app/_cs/_publish/partpub.html',
        controller: 'partpubctrl'
    })
    .state('cs.publish.published', {
        url: '/published',
        templateUrl: 'app/_cs/_publish/published.html',
        controller: 'publishedctrl'
    })
    .state('cs.publish.unpublished', {
        url: '/unpublished',
        templateUrl: 'app/_cs/_publish/unpublished.html',
        controller: 'unpublishedctrl'
    })
    .state('cs.publish.reqtounpub', {
        url: '/reqtounpub',
        templateUrl: 'app/_cs/_publish/reqtounpub.html',
        controller: 'reqtounpubctrl'
    })
    .state('cs.publish.reqtorepub', {
        url: '/reqtorepub',
        templateUrl: 'app/_cs/_publish/reqtorepub.html',
        controller: 'reqtorepubctrl'
    })
    .state('cs.publish.unpublishinprogress', {
        url: '/unpublishinprogress',
        templateUrl: 'app/_cs/_publish/unpublishinprogress.html',
        controller: 'unpublishinprogressctrl'
    })
    .state('cs.publish.reference', {
        url: '/reference',
        templateUrl: 'app/_cs/_publish/reference.html',
        controller: 'referencectrl'
    })
    .state('cs.publish.allapps', {
        url: '/allapps',
        templateUrl: 'app/_cs/_publish/allapps.html',
        controller: 'allappsctrl'
    })
    .state('cs.delete', {
        url: '/delete',
        templateUrl: 'app/_cs/_delete/delete.html',
        controller: 'deletectrl'
    })
    .state('cs.allapps', {
        params: {searchappid: null, searchappname: null, searchappstatus: null, searchskip: null},
        url: '/allapps',
        templateUrl: 'app/_cs/_allapps/allapps.html',
        controller: 'allappsctrl'
    })
    .state('cs.allapps.app', {
        abstract: true,
        url: '/app/:selectappid',
        templateUrl: 'app/_cs/_allapps/app.html',
        controller: 'appctrl'
    })
    .state('cs.allapps.app.appoverview', {
        url: '/appoverview',
        templateUrl: 'app/_cs/_allapps/_appoverview/appoverview.html',
        controller: 'appoverviewctrl'
    })
    .state('cs.allapps.app.pushsettings', {
        url: '/pushsettings',
        templateUrl: 'app/_cs/_allapps/_pushsettings/pushsettings.html',
        controller: 'pushsettingsctrl'
    })
    .state('cs.interactions', {
        abstract: true,
        url: '/interactions/:sectionid',
        templateUrl: 'app/_cs/_interact/interactions.html',
        controller: 'interactionsctrl'
    })
    .state('cs.interactions.interactionslist', {
        url: '/list',
        templateUrl: 'app/_cs/_interact/_interaction/interactionslist.html',
        controller: 'interactionslistctrl'
    })
    .state('cs.interactions.newinteractionmessage', {
        url: '/compose',
        templateUrl: 'app/_cs/_interact/_interaction/newinteractionmessage.html',
        controller: 'newinteractionmessagectrl'
    })
    .state('cs.interactions.interactionmessages', {
        abstract: true,
        url: '/messages/:interactionid',
        templateUrl: 'app/_cs/_interact/_interaction/interactionmessages.html',
        controller: 'interactionmessagesctrl'
    })
    .state('cs.interactions.interactionmessages.messageslist', {
        url: '/list',
        templateUrl: 'app/_cs/_interact/_interaction/_messages/interactionmessageslist.html',
        controller: 'interactionmessageslistctrl'
    })
    .state('cs.updateinteractmessageform', {
        url: '/updateinteractmessageform/:sectionid',
        templateUrl: 'app/_cs/_interact/updateinteractmessageform.html',
        controller: 'updateinteractmessageformctrl'
    })
    .state('cs.viewplans', {
        url: '/viewplans',
        templateUrl: 'app/_cs/_subscriptionplans/viewplans.html',
        controller: 'viewplansctrl'
    })
    .state('cs.addplan', {
        url: '/addplan',
        templateUrl: 'app/_cs/_subscriptionplans/addplan.html',
        controller: 'addplanctrl'
    })
    .state('cs.editplan', {
        url: '/editplan',
        params: {plan: null},
        templateUrl: 'app/_cs/_subscriptionplans/editplan.html',
        controller: 'editplanctrl'
    })
}
