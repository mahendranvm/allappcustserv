﻿angular.module('csapp').controller('verifyctrl', ['$scope', '$stateParams', '$state', 'Appuser', 'csconfig', function ($scope, $stateParams, $state, Appuser, csconfig) {
    $scope.user = {};
    $scope.resp = {};

    $scope.user.btnname = "Verify";
    $scope.user.btndisabled = false;
    $scope.user.btnspinner = false;

    if ($stateParams.email) {
        $scope.user.email = $stateParams.email;
    }

    $scope.verify = function () {
        $scope.resp.error = null;
        $scope.user.btnname = "Verifying...";
        $scope.user.btndisabled = true;
        $scope.user.btnspinner = true;
        $scope.verifyResult = Appuser.confirmToken({ realm: csconfig.realm, email: $scope.user.email, token: $scope.user.vcode },
         function (response) {
           $scope.resp.response = response;
           $state.go("login", { email: $scope.user.email, msg: "Registration was successful. Sign in with your credentials." });
           $scope.user.btnname = "Verify";
           $scope.user.btndisabled = false;
           $scope.user.btnspinner = false;
           }, function (error) {
           $scope.user.btnname = "Verify";
           $scope.user.btndisabled = false;
           $scope.user.btnspinner = false;
           //then check error object for custom error message, if none found show default message
           if (error && error.data && error.data.error && error.data.error.details && error.data.error.details.appErrMsg) {
              $scope.resp.error = error.data.error.details.appErrMsg;
           } else {
              $scope.resp.error = "Verification Failed. Try again later.";
           }
       });
    };

}]);
