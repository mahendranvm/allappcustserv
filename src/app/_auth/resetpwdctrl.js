﻿angular.module('csapp').controller('resetpwdctrl', ['$scope', '$stateParams', '$state', 'Appuser', 'csconfig', function ($scope, $stateParams, $state, Appuser, csconfig) {
    $scope.user = {};
    $scope.resp = {};

    $scope.user.btnname = "Reset";
    $scope.user.btndisabled = false;
    $scope.user.btnspinner = false;

    if ($stateParams.email) {
        $scope.user.email = $stateParams.email;
    }
    $scope.passreset = function () {
        $scope.resp.error = null;
        if ($scope.user.password.length >= '6') {
          if ($scope.user.password == $scope.user.confpassword) {
              $scope.user.btnname = "Requesting...";
              $scope.user.btndisabled = true;
              $scope.user.btnspinner = true;
              $scope.passresetResult = Appuser.pwdReset({credentials: { realm: csconfig.realm, email: $scope.user.email, token: $scope.user.vcode, password: $scope.user.password }},
                function (response) {
                  $scope.resp.response = response;
                  $state.go("login", { email: $scope.user.email, msg: "Password Change Successful!" });
                  $scope.user.btnname = "Reset";
                  $scope.user.btndisabled = false;
                  $scope.user.btnspinner = false;
                  }, function (error) {
                  $scope.user.btnname = "Reset";
                  $scope.user.btndisabled = false;
                  $scope.user.btnspinner = false;
                  //then check error object for custom error message, if none found show default message
                  if (error && error.data && error.data.error && error.data.error.details && error.data.error.details.appErrMsg) {
                    $scope.resp.error = error.data.error.details.appErrMsg;
                  }
                  else {
                     $scope.resp.error = "Password Reset Failed. Try again later.";
                  }
             });
          } else {
            $scope.resp.error = "Passwords not matching";
          }
        } else {
          $scope.resp.error = "Password should be at least 6 characters";
        }
    };

}]);
