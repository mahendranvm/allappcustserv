﻿angular.module('csapp').controller('loginctrl', ['$scope', '$stateParams', '$state', '$location', 'Appuser', 'csconfig', function ($scope, $stateParams, $state, $location, Appuser, csconfig) {
        $scope.user = {};
        $scope.resp = {};
        $scope.user.signinname = "Sign in";
        $scope.user.signindisabled = false;
        $scope.user.signinspinner = false;
        $scope.user.rememberMe = true;
        if ($stateParams.email) {
            $scope.user.email = $stateParams.email;
        }
        if ($stateParams.msg) {
            $scope.msg = $stateParams.msg;
        }
        $scope.login = function () {
          $scope.resp.error = null;
          $scope.user.signinname = "Signing in...";
          $scope.user.signindisabled = true;
          $scope.user.signinspinner = true;
          $scope.loginResult = Appuser.login({ rememberMe: $scope.user.rememberMe },{ realm: csconfig.realm, email: $scope.user.email.toLowerCase(), password: $scope.user.password },
           function (response) {
               $scope.resp.response = response;
               //logic from loopback angular-sdk documentation to redirect to a target page from where user was redirected to loging due to 401 error
               if ($location.nextAfterLogin) {
                 var next = $location.nextAfterLogin;
                 $location.nextAfterLogin = null;
                 $location.path(next);
               } else {
                 $state.go("cs.publish.reqtopub");
               }
               $scope.user.signinname = "Sign In";
               $scope.user.signindisabled = false;
               $scope.user.signinspinner = false;
           }, function (error) {
               $scope.user.signinname = "Sign In";
               $scope.user.signindisabled = false;
               $scope.user.signinspinner = false;
               $scope.resp.error = "Sign in Failed: Email or Password incorrect";
               $scope.msg = null;
           });
        };
}]);
