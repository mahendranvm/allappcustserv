﻿angular.module('csapp').controller('registerctrl', ['$scope', '$stateParams', '$state', 'Appuser', 'csconfig', function ($scope, $stateParams, $state, Appuser, csconfig) {
    $scope.user = {};
    $scope.resp = {};

    $scope.user.btnname = "Register";
    $scope.user.btndisabled = false;
    $scope.user.btnspinner = false;

    $scope.create = function () {
      $scope.resp.error = null;
      if ($scope.user.password.length >= '6') {
        if ($scope.user.password == $scope.user.confpassword) {
          $scope.user.btnname = "Requesting...";
          $scope.user.btndisabled = true;
          $scope.user.btnspinner = true;
          $scope.createResult = Appuser.createUser({ credentials: {realm: csconfig.realm, firstname: $scope.user.fname, lastname: $scope.user.lname, email: $scope.user.email, password: $scope.user.password }},
            function (response) {
              $scope.resp.response = response;
              $state.go("verify", { email: $scope.user.email });
              $scope.user.btnname = "Register";
              $scope.user.btndisabled = false;
              $scope.user.btnspinner = false;
            }, function (error) {
                $scope.user.btnname = "Register";
                $scope.user.btndisabled = false;
                $scope.user.btnspinner = false;
                //then check error object for custom error message, if none found show default message
                if (error && error.data && error.data.error && error.data.error.details && error.data.error.details.appErrMsg) {
                  $scope.resp.error = error.data.error.details.appErrMsg;
                }
                else {
                  $scope.resp.error = "Registration Failed. Try again later.";
                }
            });
        } else {
          $scope.resp.error = "Passwords not matching";
        }
      } else {
        $scope.resp.error = "Password should be at least 6 characters";
      }
    };

}]);
