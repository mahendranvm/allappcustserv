﻿angular.module('csapp').controller('newcodectrl', ['$scope', '$stateParams', '$state', 'Appuser', 'csconfig', function ($scope, $stateParams, $state, Appuser, csconfig) {
    $scope.user = {};
    $scope.resp = {};

    $scope.user.btnname = "Submit";
    $scope.user.btndisabled = false;
    $scope.user.btnspinner = false;

    if ($stateParams.email) {
        $scope.user.email = $stateParams.email;
    }
    $scope.newtoken = function () {
        $scope.resp.error = null;
        $scope.user.btnname = "Requesting...";
        $scope.user.btndisabled = true;
        $scope.user.btnspinner = true;
        $scope.newtokenResult = Appuser.resendVerify({ realm: csconfig.realm, email: $scope.user.email },
          function (response) {
            $scope.resp.response = response;
            $state.go("verify", { email: $scope.user.email });
            $scope.user.btnname = "Submit";
            $scope.user.btndisabled = false;
            $scope.user.btnspinner = false;
          }, function (error) {
            $scope.user.btnname = "Submit";
            $scope.user.btndisabled = false;
            $scope.user.btnspinner = false;
            //then check error object for custom error message, if none found show default message
            if (error && error.data && error.data.error && error.data.error.details && error.data.error.details.appErrMsg) {
              $scope.resp.error = error.data.error.details.appErrMsg;
            }
            else {
              $scope.resp.error = "Request for Verification Code Failed. Try again later.";
            }
        });
    };

}]);
