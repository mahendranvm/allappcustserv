﻿angular.module('csapp').controller('forgotpwdctrl', ['$scope', '$stateParams', '$state', 'Appuser', 'csconfig', function ($scope, $stateParams, $state, Appuser, csconfig) {
    $scope.user = {};
    $scope.resp = {};

    $scope.user.btnname = "Submit";
    $scope.user.btndisabled = false;
    $scope.user.btnspinner = false;

    $scope.forgotpwd = function () {
        $scope.resp.error = null;
        $scope.user.btnname = "Requesting...";
        $scope.user.btndisabled = true;
        $scope.user.btnspinner = true;
        $scope.passresetreqResult = Appuser.pwdResetreq({ realm: csconfig.realm, email: $scope.user.email },
          function (response) {
            $state.go("resetpwd",{email:$scope.user.email});
            $scope.user.btnname = "Submit";
            $scope.user.btndisabled = false;
            $scope.user.btnspinner = false;
          }, function (error) {
            $scope.user.btnname = "Submit";
            $scope.user.btndisabled = false;
            $scope.user.btnspinner = false;
            //then check error object for custom error message, if none found show default message
           if (error && error.data && error.data.error && error.data.error.details && error.data.error.details.appErrMsg) {
              $scope.resp.error = error.data.error.details.appErrMsg;
            }
           else {
              $scope.resp.error = "Request Failed. Try again later.";
            }
        });
    };
 }]);
