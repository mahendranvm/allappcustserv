angular.module('csapp').factory ('interactmoduleservice', function () {

  var tempguess = moment.tz.guess();

  //remove default territory
  var interactmoduleservice = {};

  interactmoduleservice.initiatedbyvalues = ["User", "Administrator"];

  interactmoduleservice.allowedfieldtypes = [
    "String",
    "Date",
    "Number",
    "Select One",
    "Select Multiple"
  ];

  interactmoduleservice.samplefields = [
    {"fieldname": "Name", "fieldtype": "String", "fieldrequired": true, "validvalues": []},
    {"fieldname": "Age", "fieldtype": "Number", "fieldrequired": true, "validvalues": []},
    {"fieldname": "Favorite Color", "fieldtype": "Select Multiple", "fieldrequired": false, "validvalues": ["Red", "Orange", "Blue"]}
  ];

  interactmoduleservice.standardmessage = [
    {"fieldname": "Message", "fieldtype": "String", "fieldrequired": true, "validvalues": []},
    {"fieldname": "Image", "fieldtype": "Image", "fieldrequired": false, "validvalues": []}
  ];

  interactmoduleservice.validatethisentry = function(newfield, fieldset, editthisfieldindex) {
    if (newfield.fieldtype == 'Select One' || newfield.fieldtype == 'Select Multiple') {
      if (!newfield.validvalues || newfield.validvalues.length <=0) {
        return "At least one Valid Values is required.";
      }
    }
    for (var i=0; i<fieldset.length; i++) {
      if (fieldset[i].fieldname.toLowerCase() == newfield.fieldname.toLowerCase() && i!=editthisfieldindex) {
        return "A field with this name already exists.";
      }
    }
    return null;
  };

  interactmoduleservice.validatethisfieldset = function(fieldset) {
    if (!fieldset || fieldset.length <= 0) {
      return "At least one field is required.";
    }
    return null;
  };

  interactmoduleservice.custinq = {
      "defaultvalues": {
        "initiatedby": "User",
        "admincandeleteanyinteraction": true,
        "usercandeleteowninteraction": false,
        "admincandeleteowninteraction": true,
        "firstmessageformindicator": true,
        "discussionallowed": true,
        "admincandeleteanyinteractionmessage": true,
        "admincandeleteowninteractionmessage": true,
        "usercandeleteowninteractionmessage": true,
        "otheruserforms": [],
        "otheradminforms": []
      },
      "showindicators": {
        "initiatedby": false,
        "admincandeleteanyinteraction": false,
        "usercandeleteowninteraction": false,
        "admincandeleteowninteraction": false,
        "firstmessageformindicator": false,
        "firstmessageform": true,
        "discussionallowed": false,
        "admincandeleteanyinteractionmessage": false,
        "admincandeleteowninteractionmessage": false,
        "usercandeleteowninteractionmessage": false,
        "otheruserforms": false,
        "otheradminforms": false
      }
  }

  interactmoduleservice.recvmsg = {
      "defaultvalues": {
        "initiatedby": "User",
        "admincandeleteanyinteraction": true,
        "usercandeleteowninteraction": false,
        "admincandeleteowninteraction": true,
        "firstmessageformindicator": false,
        "discussionallowed": true,
        "admincandeleteanyinteractionmessage": true,
        "admincandeleteowninteractionmessage": true,
        "usercandeleteowninteractionmessage": true,
        "otheruserforms": [],
        "otheradminforms": []
      },
      "showindicators": {
        "initiatedby": false,
        "admincandeleteanyinteraction": false,
        "usercandeleteowninteraction": false,
        "admincandeleteowninteraction": false,
        "firstmessageformindicator": false,
        "firstmessageform": false,
        "discussionallowed": false,
        "admincandeleteanyinteractionmessage": false,
        "admincandeleteowninteractionmessage": false,
        "usercandeleteowninteractionmessage": false,
        "otheruserforms": false,
        "otheradminforms": false
      }
  }

  interactmoduleservice.announce = {
      "defaultvalues": {
        "initiatedby": "Administrator",
        "admincandeleteanyinteraction": true,
        "usercandeleteowninteraction": false,
        "admincandeleteowninteraction": true,
        "firstmessageformindicator": false,
        "discussionallowed": false,
        "admincandeleteanyinteractionmessage": true,
        "admincandeleteowninteractionmessage": true,
        "usercandeleteowninteractionmessage": true,
        "otheruserforms": [],
        "otheradminforms": []
      },
      "showindicators": {
        "initiatedby": false,
        "admincandeleteanyinteraction": false,
        "usercandeleteowninteraction": false,
        "admincandeleteowninteraction": false,
        "firstmessageformindicator": false,
        "firstmessageform": false,
        "discussionallowed": false,
        "admincandeleteanyinteractionmessage": false,
        "admincandeleteowninteractionmessage": false,
        "usercandeleteowninteractionmessage": false,
        "otheruserforms": false,
        "otheradminforms": false
      }
    }

  interactmoduleservice.discuss = {
      "defaultvalues": {
        "initiatedby": "Administrator",
        "admincandeleteanyinteraction": true,
        "usercandeleteowninteraction": false,
        "admincandeleteowninteraction": true,
        "firstmessageformindicator": false,
        "discussionallowed": true,
        "admincandeleteanyinteractionmessage": true,
        "admincandeleteowninteractionmessage": true,
        "usercandeleteowninteractionmessage": true,
        "otheruserforms": [],
        "otheradminforms": []
      },
      "showindicators": {
        "initiatedby": false,
        "admincandeleteanyinteraction": false,
        "usercandeleteowninteraction": false,
        "admincandeleteowninteraction": false,
        "firstmessageformindicator": false,
        "firstmessageform": false,
        "discussionallowed": false,
        "admincandeleteanyinteractionmessage": false,
        "admincandeleteowninteractionmessage": false,
        "usercandeleteowninteractionmessage": false,
        "otheruserforms": false,
        "otheradminforms": false
      }
  }

  interactmoduleservice.reportonly = {
      "defaultvalues": {
        "initiatedby": "Administrator",
        "admincandeleteanyinteraction": true,
        "usercandeleteowninteraction": false,
        "admincandeleteowninteraction": true,
        "firstmessageformindicator": true,
        "discussionallowed": false,
        "admincandeleteanyinteractionmessage": true,
        "admincandeleteowninteractionmessage": true,
        "usercandeleteowninteractionmessage": true,
        "otheruserforms": [],
        "otheradminforms": []
      },
      "showindicators": {
        "initiatedby": false,
        "admincandeleteanyinteraction": false,
        "usercandeleteowninteraction": false,
        "admincandeleteowninteraction": false,
        "firstmessageformindicator": false,
        "firstmessageform": true,
        "discussionallowed": false,
        "admincandeleteanyinteractionmessage": false,
        "admincandeleteowninteractionmessage": false,
        "usercandeleteowninteractionmessage": false,
        "otheruserforms": false,
        "otheradminforms": false
      }
  }

  interactmoduleservice.reportdisc = {
      "defaultvalues": {
        "initiatedby": "Administrator",
        "admincandeleteanyinteraction": true,
        "usercandeleteowninteraction": false,
        "admincandeleteowninteraction": true,
        "firstmessageformindicator": true,
        "discussionallowed": true,
        "admincandeleteanyinteractionmessage": true,
        "admincandeleteowninteractionmessage": true,
        "usercandeleteowninteractionmessage": true,
        "otheruserforms": [],
        "otheradminforms": []
      },
      "showindicators": {
        "initiatedby": false,
        "admincandeleteanyinteraction": false,
        "usercandeleteowninteraction": false,
        "admincandeleteowninteraction": false,
        "firstmessageformindicator": false,
        "firstmessageform": true,
        "discussionallowed": false,
        "admincandeleteanyinteractionmessage": false,
        "admincandeleteowninteractionmessage": false,
        "usercandeleteowninteractionmessage": false,
        "otheruserforms": false,
        "otheradminforms": false
      }
  }

  return interactmoduleservice;

});
