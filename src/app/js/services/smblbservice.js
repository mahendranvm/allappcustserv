// CommonJS package manager support
if (typeof module !== 'undefined' && typeof exports !== 'undefined' &&
  module.exports === exports) {
  // Export the *name* of this Angular module
  // Sample usage:
  //
  //   import lbServices from './lb-services';
  //   angular.module('app', [lbServices]);
  //
  module.exports = "smblbservice";
}

(function(window, angular, undefined) {
  'use strict';

  var urlBase = "https://allappway.com/api";
  var authHeader = 'authorization';

  function getHost(url) {
    var m = url.match(/^(?:https?:)?\/\/([^\/]+)/);
    return m ? m[1] : null;
  }

  var urlBaseHost = getHost(urlBase) || location.host;

/**
 * @ngdoc overview
 * @name smblbservice
 * @module
 * @description
 *
 * The `smblbservice` module provides services for interacting with
 * the models exposed by the LoopBack server via the REST API.
 *
 */
  var module = angular.module("smblbservice", ['ngResource']);

/**
 * @ngdoc object
 * @name smblbservice.Appuser
 * @header smblbservice.Appuser
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Appuser` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Appuser",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/appusers/:id",
          { 'id': '@id' },
          {

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#findById
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Appuser` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/appusers/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#login
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Login a user with username/email and password.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `include` – `{string=}` - Related objects to include in the response. See the description of return value for more details.
             *   Default value: `user`.
             *
             *  - `rememberMe` - `boolean` - Whether the authentication credentials
             *     should be remembered in localStorage across app/browser restarts.
             *     Default: `true`.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * The response body contains properties of the AccessToken created on login.
             * Depending on the value of `include` parameter, the body may contain additional properties:
             *   - `user` - `U+007BUserU+007D` - Data of the currently logged in user. (`include=user`)
             *
             */
            "login": {
              params: {
                include: 'user',
              },
              interceptor: {
                response: function(response) {
                  var accessToken = response.data;
                  LoopBackAuth.setUser(
                    accessToken.id, accessToken.userId, accessToken.user);
                  LoopBackAuth.rememberMe =
                    response.config.params.rememberMe !== false;
                  LoopBackAuth.save();
                  return response.resource;
                },
              },
              url: urlBase + "/appusers/login",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#logout
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Logout a user with access token.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `access_token` – `{string=}` - Do not supply this argument, it is automatically extracted from request headers.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "logout": {
              interceptor: {
                response: function(response) {
                  LoopBackAuth.clearUser();
                  LoopBackAuth.clearStorage();
                  return response.resource;
                },
                responseError: function(responseError) {
                  LoopBackAuth.clearUser();
                  LoopBackAuth.clearStorage();
                  return responseError.resource;
                },
              },
              url: urlBase + "/appusers/logout",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$verify
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Trigger user's identity verification with configured verifyOptions
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `verifyOptions` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "prototype$verify": {
              url: urlBase + "/appusers/:id/verify",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#changePassword
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Change a user's password.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `id` – `{*=}` -
             *
             *  - `oldPassword` – `{string}` -
             *
             *  - `newPassword` – `{string}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "changePassword": {
              url: urlBase + "/appusers/change-password",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#setPassword
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Reset user's password via a password-reset token.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `id` – `{*=}` -
             *
             *  - `newPassword` – `{string}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "setPassword": {
              url: urlBase + "/appusers/reset-password",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$c_getAdminAppInfo
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get admin app
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$c_getAdminAppInfo": {
              url: urlBase + "/appusers/:id/c_getAdminAppInfo",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$c_getAllApps
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get all apps
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `appid` – `{string=}` -
             *
             *  - `appname` – `{string=}` -
             *
             *  - `appstatus` – `{*=}` -
             *
             *  - `limit` – `{number=}` -
             *
             *  - `skip` – `{number=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$c_getAllApps": {
              url: urlBase + "/appusers/:id/c_getAllApps",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$c_getAppLogs
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get App Logs
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$c_getAppLogs": {
              url: urlBase + "/appusers/:id/c_getAppLogs",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$c_createSubscriptionPlan
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Create Subscription Plan
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `subscriptionPlanInfo` – `{object}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$c_createSubscriptionPlan": {
              url: urlBase + "/appusers/:id/c_createSubscriptionPlan",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$getSubscriptionPlans
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * View Subscription Plans
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$getSubscriptionPlans": {
              url: urlBase + "/appusers/:id/getSubscriptionPlans",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$c_updateSubscriptionPlan
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update Subscription Plan
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `subscriptionPlanId` – `{string}` -
             *
             *  - `subscriptionPlanInfo` – `{object}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$c_updateSubscriptionPlan": {
              url: urlBase + "/appusers/:id/c_updateSubscriptionPlan",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$c_removeSubscriptionPlan
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Remove Subscription Plan
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `subscriptionPlanId` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$c_removeSubscriptionPlan": {
              url: urlBase + "/appusers/:id/c_removeSubscriptionPlan",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$c_updateSubscriptionCycle
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update App Status
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `subscriptiontype` – `{string}` -
             *
             *  - `trialenddate` – `{date=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$c_updateSubscriptionCycle": {
              url: urlBase + "/appusers/:id/c_updateSubscriptionCycle",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$c_updateAppStatus
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update App Status
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `status` – `{string}` -
             *
             *  - `trialenddate` – `{date=}` -
             *
             *  - `changereason` – `{string=}` -
             *
             *  - `autorenewalchange` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$c_updateAppStatus": {
              url: urlBase + "/appusers/:id/c_updateAppStatus",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$c_getAllDeletedApps
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get all deleted apps
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$c_getAllDeletedApps": {
              url: urlBase + "/appusers/:id/c_getAllDeletedApps",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$c_deleteApp
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update App Status
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$c_deleteApp": {
              url: urlBase + "/appusers/:id/c_deleteApp",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$c_updatePushSettings
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update push keys for an app
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `apnsbundleid` – `{string=}` -
             *
             *  - `gcmsenderapikey` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$c_updatePushSettings": {
              url: urlBase + "/appusers/:id/c_updatePushSettings",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#createUser
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Create new User
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `credentials` – `{object}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "createUser": {
              url: urlBase + "/appusers/createUser",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#confirmToken
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Confirm email verification token
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `realm` – `{string}` -
             *
             *  - `email` – `{string}` -
             *
             *  - `token` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "confirmToken": {
              url: urlBase + "/appusers/confirmToken",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#resendVerify
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Resend email verification token
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `realm` – `{string}` -
             *
             *  - `email` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "resendVerify": {
              url: urlBase + "/appusers/resendVerify",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#pwdResetreq
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Password Reset Request
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `realm` – `{string}` -
             *
             *  - `email` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "pwdResetreq": {
              url: urlBase + "/appusers/pwdResetreq",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#pwdReset
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Password Reset
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `credentials` – `{object}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "pwdReset": {
              url: urlBase + "/appusers/pwdReset",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$updateProfile
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update First and Last Name
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `newfirstname` – `{string}` -
             *
             *  - `newlastname` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$updateProfile": {
              url: urlBase + "/appusers/:id/updateProfile",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_requestToSubscribe
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Publish App Request
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `planitems` – `{*}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_requestToSubscribe": {
              url: urlBase + "/appusers/:id/a_requestToSubscribe",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_changeSubscriptionCard
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update Card
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `stripeToken` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_changeSubscriptionCard": {
              url: urlBase + "/appusers/:id/a_changeSubscriptionCard",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_changeAutoRenewal
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Flip Auto Renewal
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `subscriptiontype` – `{string}` -
             *
             *  - `change` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_changeAutoRenewal": {
              url: urlBase + "/appusers/:id/a_changeAutoRenewal",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getSubscriptionFromStripe
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get Latest Subscription
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getSubscriptionFromStripe": {
              url: urlBase + "/appusers/:id/a_getSubscriptionFromStripe",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getInvoicePreview
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get Invoice Preview
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `customerid` – `{string}` -
             *
             *  - `subscriptionid` – `{string}` -
             *
             *  - `planitems` – `{*}` -
             *
             *  - `prorationdate` – `{number}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getInvoicePreview": {
              url: urlBase + "/appusers/:id/a_getInvoicePreview",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_downgradeSubscriptionPlan
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Downgrade to a different set of Subscription plan
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `subscriptiontype` – `{string}` -
             *
             *  - `planitems` – `{*}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_downgradeSubscriptionPlan": {
              url: urlBase + "/appusers/:id/a_downgradeSubscriptionPlan",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_upgradeSubscriptionPlan
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Upgrade to a different set of Subscription Plans
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `subscriptiontype` – `{string}` -
             *
             *  - `planitems` – `{*}` -
             *
             *  - `prorationdate` – `{number}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_upgradeSubscriptionPlan": {
              url: urlBase + "/appusers/:id/a_upgradeSubscriptionPlan",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getLastInvoice
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get Last Invoice
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getLastInvoice": {
              url: urlBase + "/appusers/:id/a_getLastInvoice",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_payNow
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Pay Last Invoice
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `invoiceid` – `{string}` -
             *
             *  - `subscriptiontype` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_payNow": {
              url: urlBase + "/appusers/:id/a_payNow",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getAllInvoices
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get all Invoices
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `startingafter` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getAllInvoices": {
              url: urlBase + "/appusers/:id/a_getAllInvoices",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getInvoice
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get Invoice
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `invoiceid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getInvoice": {
              url: urlBase + "/appusers/:id/a_getInvoice",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$storeDeviceInfo
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Store Device Info
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `deviceinfo` – `{object}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$storeDeviceInfo": {
              url: urlBase + "/appusers/:id/storeDeviceInfo",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_sendNotification
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Send Push
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `broadcast` – `{boolean=}` -
             *
             *  - `pushtousers` – `{*=}` -
             *
             *  - `pushmessage` – `{object}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_sendNotification": {
              url: urlBase + "/appusers/:id/a_sendNotification",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getPushStatus
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Push Status
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `limit` – `{number=}` -
             *
             *  - `skip` – `{number=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getPushStatus": {
              url: urlBase + "/appusers/:id/a_getPushStatus",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_createService
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Create Service
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `name` – `{string}` -
             *
             *  - `description` – `{string=}` -
             *
             *  - `duration` – `{number}` -
             *
             *  - `contributionfactor` – `{number}` -
             *
             *  - `advancebookingperiod` – `{number=}` -
             *
             *  - `srperbooking` – `{number=}` -
             *
             *  - `multiunitbooking` – `{boolean=}` -
             *
             *  - `maxunitsperbooking` – `{number=}` -
             *
             *  - `selectresource` – `{boolean=}` -
             *
             *  - `resourcelabel` – `{string=}` -
             *
             *  - `calendarbooking` – `{boolean=}` -
             *
             *  - `paytobook` – `{string=}` -
             *
             *  - `allowcancel` – `{boolean=}` -
             *
             *  - `start` – `{date=}` -
             *
             *  - `end` – `{date=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_createService": {
              url: urlBase + "/appusers/:id/a_createService",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_createResource
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Create Resource
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `name` – `{string}` -
             *
             *  - `title` – `{string=}` -
             *
             *  - `description` – `{string=}` -
             *
             *  - `start` – `{date=}` -
             *
             *  - `end` – `{date=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_createResource": {
              url: urlBase + "/appusers/:id/a_createResource",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_setServiceResourceAvailability
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Link Services with a Resource
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `resourceid` – `{string}` -
             *
             *  - `serviceids` – `{*}` -
             *
             *  - `start` – `{date=}` -
             *
             *  - `end` – `{date=}` -
             *
             *  - `bookingstartinterval` – `{number=}` -
             *
             *  - `capacityschedule` – `{object=}` -
             *
             *  - `mode` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_setServiceResourceAvailability": {
              url: urlBase + "/appusers/:id/a_setServiceResourceAvailability",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getAllServices
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get all services for this realm and section
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `mgbooksectionid` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$a_getAllServices": {
              url: urlBase + "/appusers/:id/a_getAllServices",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getAllResources
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get all resources for this realm
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$a_getAllResources": {
              url: urlBase + "/appusers/:id/a_getAllResources",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getAllServiceResourcesWithResourceAndService
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get all serviceresources, resources and services for this realm
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getAllServiceResourcesWithResourceAndService": {
              url: urlBase + "/appusers/:id/a_getAllServiceResourcesWithResourceAndService",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$searchSRCalendarForDate
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get availability for given ServiceResources for a given date
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `mgbooksectionid` – `{string=}` -
             *
             *  - `apptimezone` – `{string=}` -
             *
             *  - `serviceresourceids` – `{*=}` -
             *
             *  - `serviceid` – `{string=}` -
             *
             *  - `tzfreequerydate` – `{date}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$searchSRCalendarForDate": {
              url: urlBase + "/appusers/:id/searchSRCalendarForDate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_searchSRCalendarForDate
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get availability for given ServiceResources for a given date - for admin use
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `mgbooksectionid` – `{string=}` -
             *
             *  - `serviceresourceids` – `{*=}` -
             *
             *  - `serviceid` – `{string=}` -
             *
             *  - `tzfreequerydate` – `{date}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$a_searchSRCalendarForDate": {
              url: urlBase + "/appusers/:id/a_searchSRCalendarForDate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$createBooking
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Book this ServiceResource, service, date and quantity provided
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `mgbooksectionid` – `{string=}` -
             *
             *  - `serviceresourcedetails` – `{*}` -
             *
             *  - `serviceid` – `{string}` -
             *
             *  - `servicename` – `{string}` -
             *
             *  - `calendarbooking` – `{boolean}` -
             *
             *  - `paytobook` – `{string}` -
             *
             *  - `tzfreequerydate` – `{date}` -
             *
             *  - `quantity` – `{number}` -
             *
             *  - `bookedtime` – `{string}` -
             *
             *  - `bookedminfrommidnight` – `{number}` -
             *
             *  - `requestedresources` – `{string}` -
             *
             *  - `assignedresources` – `{string}` -
             *
             *  - `sectionservicelabel` – `{string=}` -
             *
             *  - `apptimezone` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$createBooking": {
              url: urlBase + "/appusers/:id/createBooking",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_createBooking
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Admin Create Booking
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `mgbooksectionid` – `{string=}` -
             *
             *  - `serviceresourcedetails` – `{*}` -
             *
             *  - `serviceid` – `{string}` -
             *
             *  - `servicename` – `{string}` -
             *
             *  - `calendarbooking` – `{boolean}` -
             *
             *  - `paytobook` – `{string}` -
             *
             *  - `tzfreequerydate` – `{date}` -
             *
             *  - `quantity` – `{number}` -
             *
             *  - `bookedtime` – `{string}` -
             *
             *  - `bookedminfrommidnight` – `{number}` -
             *
             *  - `requestedresources` – `{string}` -
             *
             *  - `assignedresources` – `{string}` -
             *
             *  - `sectionservicelabel` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_createBooking": {
              url: urlBase + "/appusers/:id/a_createBooking",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$cancelBooking
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Cancel Booking and Update Calendar
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string=}` -
             *
             *  - `apptimezone` – `{string=}` -
             *
             *  - `bookingid` – `{string}` -
             *
             *  - `reason` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$cancelBooking": {
              url: urlBase + "/appusers/:id/cancelBooking",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_cancelBooking
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Cancel Booking and Update Calendar
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `bookingid` – `{string}` -
             *
             *  - `reason` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_cancelBooking": {
              url: urlBase + "/appusers/:id/a_cancelBooking",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$viewMyBookings
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * View my bookings
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `history` – `{boolean=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$viewMyBookings": {
              url: urlBase + "/appusers/:id/viewMyBookings",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getAllBooking
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get all booking
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `history` – `{boolean=}` -
             *
             *  - `limit` – `{number=}` -
             *
             *  - `skip` – `{number=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$a_getAllBooking": {
              url: urlBase + "/appusers/:id/a_getAllBooking",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getBookableSections
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get bookable sections
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$a_getBookableSections": {
              url: urlBase + "/appusers/:id/a_getBookableSections",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_updateServiceResourceCalendarCapacity
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update calendar entry for ServiceResource
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `serviceresourceid` – `{string}` -
             *
             *  - `dates` – `{date}` -
             *
             *  - `changeincapacity` – `{*}` -
             *
             *  - `changetype` – `{string}` -
             *
             *  - `bookingstartinterval` – `{number}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_updateServiceResourceCalendarCapacity": {
              url: urlBase + "/appusers/:id/a_updateServiceResourceCalendarCapacity",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_updateService
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update Service
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `serviceid` – `{string}` -
             *
             *  - `newname` – `{string=}` -
             *
             *  - `newdescription` – `{string=}` -
             *
             *  - `newduration` – `{number=}` -
             *
             *  - `newcontributionfactor` – `{number=}` -
             *
             *  - `newadvancebookingperiod` – `{number=}` -
             *
             *  - `newsrperbooking` – `{number=}` -
             *
             *  - `newmultiunitbooking` – `{boolean=}` -
             *
             *  - `newmaxunitsperbooking` – `{number=}` -
             *
             *  - `newselectresource` – `{boolean=}` -
             *
             *  - `newresourcelabel` – `{string=}` -
             *
             *  - `newcalendarbooking` – `{boolean=}` -
             *
             *  - `newpaytobook` – `{string=}` -
             *
             *  - `newallowcancel` – `{boolean=}` -
             *
             *  - `newstart` – `{date=}` -
             *
             *  - `newend` – `{date=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_updateService": {
              url: urlBase + "/appusers/:id/a_updateService",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_updateResourceInfo
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update Resource Info
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `resourceid` – `{string}` -
             *
             *  - `newname` – `{string=}` -
             *
             *  - `newtitle` – `{string=}` -
             *
             *  - `newdescription` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_updateResourceInfo": {
              url: urlBase + "/appusers/:id/a_updateResourceInfo",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_updateResourceStartOrEnd
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update Resource Start or End
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `resourceid` – `{string}` -
             *
             *  - `newdate` – `{date}` -
             *
             *  - `startorend` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_updateResourceStartOrEnd": {
              url: urlBase + "/appusers/:id/a_updateResourceStartOrEnd",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_updateServiceResourceStartOrEnd
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update Service Resource Start or End
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `serviceresourceid` – `{string}` -
             *
             *  - `newdate` – `{date}` -
             *
             *  - `startorend` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_updateServiceResourceStartOrEnd": {
              url: urlBase + "/appusers/:id/a_updateServiceResourceStartOrEnd",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_attachMessageForm
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Create a new message form
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             *  - `name` – `{string=}` -
             *
             *  - `fields` – `{*}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_attachMessageForm": {
              url: urlBase + "/appusers/:id/a_attachMessageForm",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$createInteraction
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Create a new interaction
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `title` – `{string}` -
             *
             *  - `broadcastind` – `{boolean=}` -
             *
             *  - `receiverids` – `{*=}` -
             *
             *  - `message` – `{*}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$createInteraction": {
              url: urlBase + "/appusers/:id/createInteraction",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$createInteractionMessage
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Create a new interaction message
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `interactionid` – `{string}` -
             *
             *  - `message` – `{*}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$createInteractionMessage": {
              url: urlBase + "/appusers/:id/createInteractionMessage",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$getPutSignedUrlForInteractionMessage
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Create a signed url for interaction message image
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `interactionid` – `{string=}` -
             *
             *  - `fullkey` – `{string}` -
             *
             *  - `imgtype` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$getPutSignedUrlForInteractionMessage": {
              url: urlBase + "/appusers/:id/getPutSignedUrlForInteractionMessage",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$getInteractions
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get interactions
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `limit` – `{number}` -
             *
             *  - `skip` – `{number}` -
             *
             *  - `clienttotalcount` – `{number=}` -
             *
             *  - `paginationmode` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$getInteractions": {
              url: urlBase + "/appusers/:id/getInteractions",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$getInteractionMessages
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get interaction messages
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `interactionid` – `{string}` -
             *
             *  - `limit` – `{number}` -
             *
             *  - `skip` – `{number}` -
             *
             *  - `clienttotalcount` – `{number=}` -
             *
             *  - `paginationmode` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$getInteractionMessages": {
              url: urlBase + "/appusers/:id/getInteractionMessages",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$updateInteractionUserTracker
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update interaction unread count tracker
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `interactionid` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `reduceby` – `{number}` -
             *
             *  - `lastreadmessagedate` – `{date}` -
             *
             *  - `updateonbookmark` – `{date}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$updateInteractionUserTracker": {
              url: urlBase + "/appusers/:id/updateInteractionUserTracker",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$deleteInteraction
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Delete interaction
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `interactionid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$deleteInteraction": {
              url: urlBase + "/appusers/:id/deleteInteraction",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$deleteInteractionMessage
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Delete interaction message
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `interactionid` – `{string}` -
             *
             *  - `interactionmessageid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$deleteInteractionMessage": {
              url: urlBase + "/appusers/:id/deleteInteractionMessage",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$getInteractionsV2
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get interaction message v2
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `paginationmode` – `{string}` -
             *
             *  - `bookmarkdate` – `{date}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$getInteractionsV2": {
              url: urlBase + "/appusers/:id/getInteractionsV2",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$getInteractionMessagesV2
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get interaction message v2
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `interactionid` – `{string}` -
             *
             *  - `paginationmode` – `{string}` -
             *
             *  - `bookmarkdate` – `{date}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$getInteractionMessagesV2": {
              url: urlBase + "/appusers/:id/getInteractionMessagesV2",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getSignedUrlForAttachment
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get interaction message attachment download url
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `interactionid` – `{string}` -
             *
             *  - `interactionmessageid` – `{string}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             *  - `fullfilekey` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getSignedUrlForAttachment": {
              url: urlBase + "/appusers/:id/a_getSignedUrlForAttachment",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_createAlbum
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Create a new album
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `albumname` – `{string}` -
             *
             *  - `broadcastind` – `{boolean=}` -
             *
             *  - `receiverids` – `{*=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_createAlbum": {
              url: urlBase + "/appusers/:id/a_createAlbum",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_updateAlbum
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update an existing album
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `albumid` – `{string}` -
             *
             *  - `albumname` – `{string}` -
             *
             *  - `broadcastind` – `{boolean=}` -
             *
             *  - `receiverids` – `{*=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_updateAlbum": {
              url: urlBase + "/appusers/:id/a_updateAlbum",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_removeAlbum
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Remove a new album
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `albumid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_removeAlbum": {
              url: urlBase + "/appusers/:id/a_removeAlbum",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_uploadPicUrl
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get URL for adding a new pic
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `filekeyprefix` – `{string}` -
             *
             *  - `sizeoptions` – `{*}` -
             *
             *  - `imgkey` – `{string}` -
             *
             *  - `imgtype` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$a_uploadPicUrl": {
              url: urlBase + "/appusers/:id/a_uploadPicUrl",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$getAlbums
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get albums in section
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$getAlbums": {
              url: urlBase + "/appusers/:id/getAlbums",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$listObjectsAWS
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * List pics in an album
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `albumid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$listObjectsAWS": {
              url: urlBase + "/appusers/:id/listObjectsAWS",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$getSignedUrlAWS
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get url for pics in an album
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `albumid` – `{string}` -
             *
             *  - `fullkeys` – `{*}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$getSignedUrlAWS": {
              url: urlBase + "/appusers/:id/getSignedUrlAWS",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$removePics
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Remove pics from an album
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `fullkeys` – `{*}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$removePics": {
              url: urlBase + "/appusers/:id/removePics",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_addAppSection
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Create a new section in the app
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionname` – `{string}` -
             *
             *  - `appmodule` – `{string}` -
             *
             *  - `usermodulename` – `{string}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             *  - `details` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_addAppSection": {
              url: urlBase + "/appusers/:id/a_addAppSection",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_updateAppSection
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update an existing section in the app
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `sectionname` – `{string}` -
             *
             *  - `appmodule` – `{string}` -
             *
             *  - `usermodulename` – `{string}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             *  - `details` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_updateAppSection": {
              url: urlBase + "/appusers/:id/a_updateAppSection",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_enableDisableAppSection
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Remove an existing section in the app
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `change` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_enableDisableAppSection": {
              url: urlBase + "/appusers/:id/a_enableDisableAppSection",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_removeAppSection
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Remove an existing section in the app
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `sectionname` – `{string}` -
             *
             *  - `appmodule` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_removeAppSection": {
              url: urlBase + "/appusers/:id/a_removeAppSection",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getCustomEntityData
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Add a new role
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             *  - `archiveind` – `{boolean=}` -
             *
             *  - `viewname` – `{string}` -
             *
             *  - `limit` – `{number=}` -
             *
             *  - `skip` – `{number=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getCustomEntityData": {
              url: urlBase + "/appusers/:id/a_getCustomEntityData",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_addCustomEntityData
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Add a new role
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `relatedsectiondata` – `{*=}` -
             *
             *  - `fielddata` – `{*}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_addCustomEntityData": {
              url: urlBase + "/appusers/:id/a_addCustomEntityData",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_editCustomEntityData
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Add a new role
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `entityid` – `{string}` -
             *
             *  - `relatedsectiondata` – `{*=}` -
             *
             *  - `fielddata` – `{*}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_editCustomEntityData": {
              url: urlBase + "/appusers/:id/a_editCustomEntityData",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_deleteCustomEntityData
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Add a new role
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `entityid` – `{string}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_deleteCustomEntityData": {
              url: urlBase + "/appusers/:id/a_deleteCustomEntityData",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_archiveUpdateCustomEntityData
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Add a new role
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `entityid` – `{string}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             *  - `archiveind` – `{boolean}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_archiveUpdateCustomEntityData": {
              url: urlBase + "/appusers/:id/a_archiveUpdateCustomEntityData",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_addCEDraftAttachment
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Add a new attachment
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `filename` – `{string}` -
             *
             *  - `filetype` – `{string=}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             *  - `entityid` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_addCEDraftAttachment": {
              url: urlBase + "/appusers/:id/a_addCEDraftAttachment",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_discardCEDraftAttachment
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Discard a new attachment
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `delids` – `{*}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_discardCEDraftAttachment": {
              url: urlBase + "/appusers/:id/a_discardCEDraftAttachment",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_downloadCustomEntityAttachment
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Download attachment
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `entityid` – `{string}` -
             *
             *  - `fieldcmdbname` – `{string}` -
             *
             *  - `attachmentid` – `{string}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_downloadCustomEntityAttachment": {
              url: urlBase + "/appusers/:id/a_downloadCustomEntityAttachment",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_updateCustomEntityViews
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Add a new role
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `customentityviews` – `{*}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_updateCustomEntityViews": {
              url: urlBase + "/appusers/:id/a_updateCustomEntityViews",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_shareCustomEntityViewForComments
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Post Custom Entity to another section
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `entityid` – `{string}` -
             *
             *  - `viewname` – `{string}` -
             *
             *  - `posttosectionid` – `{string}` -
             *
             *  - `title` – `{string}` -
             *
             *  - `broadcastind` – `{boolean=}` -
             *
             *  - `receiverids` – `{*=}` -
             *
             *  - `usermodkey` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_shareCustomEntityViewForComments": {
              url: urlBase + "/appusers/:id/a_shareCustomEntityViewForComments",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_addRole
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Add a new role
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `name` – `{string}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `rights` – `{*}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_addRole": {
              url: urlBase + "/appusers/:id/a_addRole",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_removeRole
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Remove role
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `roleid` – `{string}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_removeRole": {
              url: urlBase + "/appusers/:id/a_removeRole",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_editRole
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Edit role
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `roleid` – `{string}` -
             *
             *  - `name` – `{string}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `rights` – `{*}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_editRole": {
              url: urlBase + "/appusers/:id/a_editRole",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getRoles
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get roles
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$a_getRoles": {
              url: urlBase + "/appusers/:id/a_getRoles",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_addStaff
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Add staff
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `staffemail` – `{string}` -
             *
             *  - `roleid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_addStaff": {
              url: urlBase + "/appusers/:id/a_addStaff",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_removeStaff
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Remove staff
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `staffid` – `{string}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `staffemail` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_removeStaff": {
              url: urlBase + "/appusers/:id/a_removeStaff",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_editStaff
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Edit staff
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `staffid` – `{string}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `staffemail` – `{string}` -
             *
             *  - `roleid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_editStaff": {
              url: urlBase + "/appusers/:id/a_editStaff",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getStaffAndRoles
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get staff and roles
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getStaffAndRoles": {
              url: urlBase + "/appusers/:id/a_getStaffAndRoles",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_acceptStaffInvitation
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Accept staff invite
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `staffid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_acceptStaffInvitation": {
              url: urlBase + "/appusers/:id/a_acceptStaffInvitation",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_reminderStaffInvitation
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Send reminder staff invite
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `staffid` – `{string}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_reminderStaffInvitation": {
              url: urlBase + "/appusers/:id/a_reminderStaffInvitation",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_createApp
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Add a new app
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `appname` – `{string}` -
             *
             *  - `apptimezone` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_createApp": {
              url: urlBase + "/appusers/:id/a_createApp",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_updateApp
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update an existing app
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `newappname` – `{string=}` -
             *
             *  - `newapptimezone` – `{string=}` -
             *
             *  - `newappconfig` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_updateApp": {
              url: urlBase + "/appusers/:id/a_updateApp",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_updateAppStatus
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Request to Publish an existing app
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `status` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_updateAppStatus": {
              url: urlBase + "/appusers/:id/a_updateAppStatus",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getMyApps
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get my apps
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$a_getMyApps": {
              url: urlBase + "/appusers/:id/a_getMyApps",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getMyAppDetail
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get my app detail
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$a_getMyAppDetail": {
              url: urlBase + "/appusers/:id/a_getMyAppDetail",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_markAppForDeletion
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Mark App for Deletion
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_markAppForDeletion": {
              url: urlBase + "/appusers/:id/a_markAppForDeletion",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_undoAppDeletion
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Undo App deletion
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_undoAppDeletion": {
              url: urlBase + "/appusers/:id/a_undoAppDeletion",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getMyDeletedApps
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get my deleted apps
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$a_getMyDeletedApps": {
              url: urlBase + "/appusers/:id/a_getMyDeletedApps",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getRegisteredAppUsers
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get registered users of given app
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string=}` -
             *
             *  - `usermodkey` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$a_getRegisteredAppUsers": {
              url: urlBase + "/appusers/:id/a_getRegisteredAppUsers",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getRecipientUsers
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get registered users of given app
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string=}` -
             *
             *  - `usermodkey` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$a_getRecipientUsers": {
              url: urlBase + "/appusers/:id/a_getRecipientUsers",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getRegisteredAppUsersCount
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get registered users count of given app
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getRegisteredAppUsersCount": {
              url: urlBase + "/appusers/:id/a_getRegisteredAppUsersCount",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#getAppInfo
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get app info
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "getAppInfo": {
              url: urlBase + "/appusers/getAppInfo",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$getBookingSectionWithServiceDetails
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get app booking section details
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$getBookingSectionWithServiceDetails": {
              url: urlBase + "/appusers/:id/getBookingSectionWithServiceDetails",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getFromImageRepositoryListObjects
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get list of pics from image repository
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getFromImageRepositoryListObjects": {
              url: urlBase + "/appusers/:id/a_getFromImageRepositoryListObjects",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getAppSplashScreenSignedUrl
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get splash screen image url
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getAppSplashScreenSignedUrl": {
              url: urlBase + "/appusers/:id/a_getAppSplashScreenSignedUrl",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getAppIconImageSignedUrl
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get app icon image url
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_getAppIconImageSignedUrl": {
              url: urlBase + "/appusers/:id/a_getAppIconImageSignedUrl",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#getFromImageRepositorySignedUrl
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get signed urls for pics
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `realm` – `{string}` -
             *
             *  - `fullkeys` – `{*}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "getFromImageRepositorySignedUrl": {
              url: urlBase + "/appusers/getFromImageRepositorySignedUrl",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$getStaticSectionData
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get data for given static section
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string=}` -
             *
             *  - `sectionid` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$getStaticSectionData": {
              url: urlBase + "/appusers/:id/getStaticSectionData",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_updateStaticSectionData
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Update Static Section Data - For use in 1 record per section type sections
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `sectionid` – `{string}` -
             *
             *  - `sectiondata` – `{*=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$a_updateStaticSectionData": {
              url: urlBase + "/appusers/:id/a_updateStaticSectionData",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$a_getDashboardDetails
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get Data for Dashboard
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             *  - `pushsectionid` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{*}` -
             */
            "prototype$a_getDashboardDetails": {
              url: urlBase + "/appusers/:id/a_getDashboardDetails",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#prototype$getAppBadgeInfo
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get Data for Dashboard
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - appuser id
             *
             *  - `options` – `{object=}` -
             *
             *  - `realm` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "prototype$getAppBadgeInfo": {
              url: urlBase + "/appusers/:id/getAppBadgeInfo",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#test_getVerifyToken
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Test function to get verification token
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `realm` – `{string}` -
             *
             *  - `email` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `success` – `{object}` -
             */
            "test_getVerifyToken": {
              url: urlBase + "/appusers/test_getVerifyToken",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name smblbservice.Appuser#getCurrent
             * @methodOf smblbservice.Appuser
             *
             * @description
             *
             * Get data of the currently logged user. Fail with HTTP result 401
             * when there is no user logged in.
             *
             * @param {function(Object,Object)=} successCb
             *    Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *    `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             */
            'getCurrent': {
              url: urlBase + "/appusers" + '/:id',
              method: 'GET',
              params: {
                id: function() {
                  var id = LoopBackAuth.currentUserId;
                  if (id == null) id = '__anonymous__';
                  return id;
                },
              },
              interceptor: {
                response: function(response) {
                  LoopBackAuth.currentUserData = response.data;
                  return response.resource;
                },
                responseError: function(responseError) {
                  LoopBackAuth.clearUser();
                  LoopBackAuth.clearStorage();
                  return $q.reject(responseError);
                },
              },
              __isGetCurrentUser__: true,
            },
          }
        );



        /**
         * @ngdoc method
         * @name smblbservice.Appuser#getCachedCurrent
         * @methodOf smblbservice.Appuser
         *
         * @description
         *
         * Get data of the currently logged user that was returned by the last
         * call to {@link smblbservice.Appuser#login} or
         * {@link smblbservice.Appuser#getCurrent}. Return null when there
         * is no user logged in or the data of the current user were not fetched
         * yet.
         *
         * @returns {Object} A Appuser instance.
         */
        R.getCachedCurrent = function() {
          var data = LoopBackAuth.currentUserData;
          return data ? new R(data) : null;
        };

        /**
         * @ngdoc method
         * @name smblbservice.Appuser#isAuthenticated
         * @methodOf smblbservice.Appuser
         *
         * @returns {boolean} True if the current user is authenticated (logged in).
         */
        R.isAuthenticated = function() {
          return this.getCurrentId() != null;
        };

        /**
         * @ngdoc method
         * @name smblbservice.Appuser#getCurrentId
         * @methodOf smblbservice.Appuser
         *
         * @returns {Object} Id of the currently logged-in user or null.
         */
        R.getCurrentId = function() {
          return LoopBackAuth.currentUserId;
        };

        /**
        * @ngdoc property
        * @name smblbservice.Appuser#modelName
        * @propertyOf smblbservice.Appuser
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Appuser`.
        */
        R.modelName = "Appuser";



        return R;
      }]);


  module
  .factory('LoopBackAuth', function() {
    var props = ['accessTokenId', 'currentUserId', 'rememberMe'];
    var propsPrefix = '$LoopBack$';

    function LoopBackAuth() {
      var self = this;
      props.forEach(function(name) {
        self[name] = load(name);
      });
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.save = function() {
      var self = this;
      var storage = this.rememberMe ? localStorage : sessionStorage;
      props.forEach(function(name) {
        save(storage, name, self[name]);
      });
    };

    LoopBackAuth.prototype.setUser = function(accessTokenId, userId, userData) {
      this.accessTokenId = accessTokenId;
      this.currentUserId = userId;
      this.currentUserData = userData;
    };

    LoopBackAuth.prototype.clearUser = function() {
      this.accessTokenId = null;
      this.currentUserId = null;
      this.currentUserData = null;
    };

    LoopBackAuth.prototype.clearStorage = function() {
      props.forEach(function(name) {
        save(sessionStorage, name, null);
        save(localStorage, name, null);
      });
    };

    return new LoopBackAuth();

    // Note: LocalStorage converts the value to string
    // We are using empty string as a marker for null/undefined values.
    function save(storage, name, value) {
      try {
        var key = propsPrefix + name;
        if (value == null) value = '';
        storage[key] = value;
      } catch (err) {
        console.log('Cannot access local/session storage:', err);
      }
    }

    function load(name) {
      var key = propsPrefix + name;
      return localStorage[key] || sessionStorage[key] || null;
    }
  })
  .config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('LoopBackAuthRequestInterceptor');
  }])
  .factory('LoopBackAuthRequestInterceptor', ['$q', 'LoopBackAuth',
    function($q, LoopBackAuth) {
      return {
        'request': function(config) {
          // filter out external requests
          var host = getHost(config.url);
          if (host && host !== urlBaseHost) {
            return config;
          }

          if (LoopBackAuth.accessTokenId) {
            config.headers[authHeader] = LoopBackAuth.accessTokenId;
          } else if (config.__isGetCurrentUser__) {
            // Return a stub 401 error for User.getCurrent() when
            // there is no user logged in
            var res = {
              body: { error: { status: 401 }},
              status: 401,
              config: config,
              headers: function() { return undefined; },
            };
            return $q.reject(res);
          }
          return config || $q.when(config);
        },
      };
    }])

  /**
   * @ngdoc object
   * @name smblbservice.LoopBackResourceProvider
   * @header smblbservice.LoopBackResourceProvider
   * @description
   * Use `LoopBackResourceProvider` to change the global configuration
   * settings used by all models. Note that the provider is available
   * to Configuration Blocks only, see
   * {@link https://docs.angularjs.org/guide/module#module-loading-dependencies Module Loading & Dependencies}
   * for more details.
   *
   * ## Example
   *
   * ```js
   * angular.module('app')
   *  .config(function(LoopBackResourceProvider) {
   *     LoopBackResourceProvider.setAuthHeader('X-Access-Token');
   *  });
   * ```
   */
  .provider('LoopBackResource', function LoopBackResourceProvider() {
    /**
     * @ngdoc method
     * @name smblbservice.LoopBackResourceProvider#setAuthHeader
     * @methodOf smblbservice.LoopBackResourceProvider
     * @param {string} header The header name to use, e.g. `X-Access-Token`
     * @description
     * Configure the REST transport to use a different header for sending
     * the authentication token. It is sent in the `Authorization` header
     * by default.
     */
    this.setAuthHeader = function(header) {
      authHeader = header;
    };

    /**
     * @ngdoc method
     * @name smblbservice.LoopBackResourceProvider#getAuthHeader
     * @methodOf smblbservice.LoopBackResourceProvider
     * @description
     * Get the header name that is used for sending the authentication token.
     */
    this.getAuthHeader = function() {
      return authHeader;
    };

    /**
     * @ngdoc method
     * @name smblbservice.LoopBackResourceProvider#setUrlBase
     * @methodOf smblbservice.LoopBackResourceProvider
     * @param {string} url The URL to use, e.g. `/api` or `//example.com/api`.
     * @description
     * Change the URL of the REST API server. By default, the URL provided
     * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
     */
    this.setUrlBase = function(url) {
      urlBase = url;
      urlBaseHost = getHost(urlBase) || location.host;
    };

    /**
     * @ngdoc method
     * @name smblbservice.LoopBackResourceProvider#getUrlBase
     * @methodOf smblbservice.LoopBackResourceProvider
     * @description
     * Get the URL of the REST API server. The URL provided
     * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
     */
    this.getUrlBase = function() {
      return urlBase;
    };

    this.$get = ['$resource', function($resource) {
      var LoopBackResource = function(url, params, actions) {
        var resource = $resource(url, params, actions);

        // Angular always calls POST on $save()
        // This hack is based on
        // http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/
        resource.prototype.$save = function(success, error) {
          // Fortunately, LoopBack provides a convenient `upsert` method
          // that exactly fits our needs.
          var result = resource.upsert.call(this, {}, this, success, error);
          return result.$promise || result;
        };
        return resource;
      };

      LoopBackResource.getUrlBase = function() {
        return urlBase;
      };

      LoopBackResource.getAuthHeader = function() {
        return authHeader;
      };

      return LoopBackResource;
    }];
  });
})(window, window.angular);
