angular.module("csapp")
.config(['$mdThemingProvider', '$httpProvider', '$compileProvider', '$provide', function ($mdThemingProvider, $httpProvider, $compileProvider, $provide) {

    $mdThemingProvider.theme('default')
    .primaryPalette('green', {
      'default': '600',
      'hue-1': '300',
      'hue-2': '800',
      'hue-3': 'A100'
    })
    .accentPalette('deep-orange');


    //this line will enable datapicker. see issue and solution here https://github.com/angular/material/issues/10168
    $compileProvider.preAssignBindingsEnabled(true);

    // logic to redirect to login on 401 error
    $httpProvider.interceptors.push(function($q, $location, LoopBackAuth) {
      return {
        responseError: function(rejection) {
          if (rejection.status == 401 || rejection.statusCode == 401) {
            //Now clearing the loopback values from client browser for safe logout...
            LoopBackAuth.clearUser();
            LoopBackAuth.clearStorage();
            //do this redirect only if the app is inside cs state, if in auth state nothing to be done
            if ($location.path().search('/cs/') === 0) {
              console.log("logic to redirect to login on 401 error");
              $location.nextAfterLogin = $location.path();
              $location.path('/login');
            }
          }
          return $q.reject(rejection);
        }
      };
    });
}])

.run(['$rootScope', '$location', 'LoopBackAuth', function ($rootScope, $location, LoopBackAuth) {
//logic to check if a user id and auth token is available
  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options){
    if (!LoopBackAuth.currentUserId || LoopBackAuth.currentUserId===''||
        !LoopBackAuth.accessTokenId || LoopBackAuth.accessTokenId==='') {
       //Now clearing the loopback values from client browser for safe logout...
       LoopBackAuth.clearUser();
       LoopBackAuth.clearStorage();
       //do this redirect only if the app is inside cs state, if in auth state nothing to be done
       if ($location.path().search('/cs/') === 0) {
         $location.nextAfterLogin = $location.path();
         $location.path('/login');
       }
    }
  });
}]);
