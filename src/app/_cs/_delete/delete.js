angular.module('csapp').controller('deletectrl', ['$scope', '$state', 'csconfig', 'Appuser', 'LoopBackAuth',  function ($scope, $state, csconfig, Appuser, LoopBackAuth) {

  csconfig.mode = "delete";

  $scope.vardelete = {};

  $scope.vardelete.loading = true;
  $scope.vardelete.apps = true;
  $scope.vardelete.noapp = false;
  $scope.vardelete.error = null;

  function initload () {
    $scope.cs.showprogress = true;
    Appuser.prototype$c_getAllDeletedApps({ id: LoopBackAuth.currentUserId},
      function (response) {
          if (response.success && response.success.length) {
            $scope.vardelete.apps = response.success;
            $scope.vardelete.noapp = false;
          } else {
            $scope.vardelete.noapp = true;
          }
          $scope.cs.showprogress = false;
          $scope.vardelete.loading = false;
      }, function (error) {
          $scope.vardelete.error = "Error getting apps. Try again later.";
          $scope.cs.showprogress = false;
          $scope.vardelete.loading = false;
      });
  }

  $scope.deletepermanent = function (realm) {
    $scope.cs.showprogress = true;
    Appuser.prototype$c_deleteApp({ id: LoopBackAuth.currentUserId, realm: realm},
      function (response) {
        $scope.cs.showprogress = false;
        $scope.vardelete.loading = false;
        $state.go ("cs.delete",{},{reload: true})
      }, function (error) {
        $scope.vardelete.error = "Error Deleting.";
        $scope.cs.showprogress = false;
        $scope.vardelete.loading = false;
      });
  }

  initload();

}]);
