angular.module('csapp').controller('publishctrl', ['$scope', '$state', 'csconfig', 'Appuser', 'LoopBackAuth', function ($scope, $state, csconfig, Appuser, LoopBackAuth) {

  csconfig.mode = "publish";

  $scope.goto = function (page) {
    if (page == 'notpublished') $state.go ("cs.publish.notpublished")
    else if (page == 'unpublished') $state.go ("cs.publish.unpublished")
    else if (page == 'reqtopub') $state.go ("cs.publish.reqtopub")
    else if (page == 'reqtorepub') $state.go ("cs.publish.reqtorepub")
    else if (page == 'package') $state.go ("cs.publish.package")
    else if (page == 'toappstores') $state.go ("cs.publish.toappstores")
    else if (page == 'partpub') $state.go ("cs.publish.partpub")
    else if (page == 'published') $state.go ("cs.publish.published")
    else if (page == 'reqtounpub') $state.go ("cs.publish.reqtounpub")
    else if (page == 'unpublishinprogress') $state.go ("cs.publish.unpublishinprogress")
    else if (page == 'reference') $state.go ("cs.publish.reference")
  }

  $scope.publish = {};
  $scope.publish.currentnavitem = null;


  $scope.getAllApps = function (pagevar) {
    $scope.cs.showprogress = true;
    Appuser.prototype$c_getAllApps({ id: LoopBackAuth.currentUserId, appstatus: pagevar.pageappstatus, limit: pagevar.limit, skip: pagevar.skip},
      function (response) {
          if (response.success && response.success.apps && response.success.apps.length) {
            pagevar.apps = response.success.apps;
            pagevar.appcount = response.success.appcount;
            pagevar.noapp = false;
          } else {
            pagevar.noapp = true;
          }
          $scope.cs.showprogress = false;
          pagevar.loading = false;
      }, function (error) {
          pagevar.error = "Error getting apps. Try again later.";
          $scope.cs.showprogress = false;
          pagevar.loading = false;
      });
  }

  $scope.next = function (pagevar) {
    pagevar.skip = pagevar.skip + pagevar.limit;
    $scope.getAllApps(pagevar);
  }

  $scope.previous = function (pagevar) {
    pagevar.skip = pagevar.skip - pagevar.limit;
    $scope.getAllApps(pagevar);
  }

}]);
