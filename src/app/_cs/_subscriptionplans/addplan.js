angular.module('csapp').controller('addplanctrl', ['$scope', '$state', '$stateParams', 'Appuser', 'LoopBackAuth', 'csconfig', function ($scope, $state, $stateParams, Appuser, LoopBackAuth, csconfig) {

  csconfig.mode = "plans";

  $scope.varaddplan = {};
  $scope.varaddplan.plantypes = ["bpsubscription", "casubscription"]
  $scope.varaddplan.createbtndisabled = false;
  $scope.varaddplan.plan = {
    type: "bpsubscription",
    recommended: false,
  };
  $scope.varaddplan.error = null;

  $scope.addplan = function () {
    $scope.cs.showprogress = true;
    $scope.varaddplan.createbtndisabled = true;
    Appuser.prototype$c_createSubscriptionPlan( { id: LoopBackAuth.currentUserId, subscriptionPlanInfo: $scope.varaddplan.plan } ,
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.viewplans", {}, {reload: true});
      }, function (error) {
          $scope.varaddplan.error = "Error adding plan. Try again later.";
          $scope.cs.showprogress = false;
          $scope.varaddplan.createbtndisabled = false;
      });
  }


}]);
