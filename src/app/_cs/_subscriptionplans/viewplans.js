angular.module('csapp').controller('viewplansctrl', ['$scope', '$state', '$stateParams', '$mdDialog', 'Appuser', 'LoopBackAuth', 'csconfig', function ($scope, $state, $stateParams, $mdDialog, Appuser, LoopBackAuth, csconfig) {

  csconfig.mode = "plans";

  $scope.varsubplans = {};
  $scope.varsubplans.plans = null;
  $scope.varsubplans.noplan = false;
  $scope.varsubplans.loading = true;
  $scope.varsubplans.error = null;

  $scope.cs.showprogress = true;
  Appuser.prototype$getSubscriptionPlans( { id: LoopBackAuth.currentUserId } ,
    function (response) {
        if (response.success && response.success.length) {
          $scope.varsubplans.plans = response.success;
          $scope.varsubplans.noplan = false;
        } else {
          $scope.varsubplans.noplan = true;
        }
        $scope.cs.showprogress = false;
        $scope.varsubplans.loading = false;
    }, function (error) {
        $scope.varsubplans.error = "Error getting plans. Try again later.";
        $scope.cs.showprogress = false;
        $scope.varsubplans.loading = false;
    });

  $scope.editplan = function (plan) {
    $state.go ("cs.editplan", {plan: plan});
  }

  $scope.deleteplan = function (plan, ev) {

    $mdDialog.show(
        $mdDialog.confirm()
          .clickOutsideToClose(true)
          .title("Plan will be deleted permanently.")
          .textContent("Users will no longer be able to subscribe to this plan. Are you sure you want to delete this plan?")
          .ok("Delete")
          .cancel("Cancel")
          .targetEvent(ev)
      ).then(function() {
        $scope.cs.showprogress = true;
        Appuser.prototype$c_removeSubscriptionPlan( { id: LoopBackAuth.currentUserId, subscriptionPlanId: plan.id } ,
          function (response) {
              $scope.cs.showprogress = false;
              $state.go ("cs.viewplans", {}, {reload: true});
          }, function (error) {
              $scope.varsubplans.error = "Error removing plan. Try again later.";
              $scope.cs.showprogress = false;
          });
      }, function() {
        //do nothing on cancel
      });
  }


}]);
