angular.module('csapp').controller('editplanctrl', ['$scope', '$state', '$stateParams', 'Appuser', 'LoopBackAuth', 'csconfig', function ($scope, $state, $stateParams, Appuser, LoopBackAuth, csconfig) {

  csconfig.mode = "plans";

  $scope.vareditplan = {};
  $scope.vareditplan.plantypes = ["bpsubscription", "casubscription"]
  $scope.vareditplan.changebtndisabled = false;
  $scope.vareditplan.plan = {
    type: "bpsubscription",
    recommended: false,
  };
  $scope.vareditplan.error = null;

  if (!$stateParams.plan) {
    $state.go ("cs.viewplans", {}, {reload: true});
  } else {
    $scope.vareditplan.plan = $stateParams.plan;
  }

  $scope.changeplan = function () {
    $scope.cs.showprogress = true;
    $scope.vareditplan.createbtndisabled = true;
    Appuser.prototype$c_updateSubscriptionPlan( { id: LoopBackAuth.currentUserId, subscriptionPlanId: $scope.vareditplan.plan.id, subscriptionPlanInfo: $scope.vareditplan.plan } ,
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.viewplans", {}, {reload: true});
      }, function (error) {
          $scope.vareditplan.error = "Error changing plan. Try again later.";
          $scope.cs.showprogress = false;
          $scope.vareditplan.createbtndisabled = false;
      });
  }


}]);
