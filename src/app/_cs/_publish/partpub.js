angular.module('csapp').controller('partpubctrl', ['$scope', '$state', 'Appuser', 'LoopBackAuth', '$mdDialog', function ($scope, $state, Appuser, LoopBackAuth, $mdDialog) {

  $scope.publish.currentnavitem = 'partpub';
  $scope.varpartpub = {};

  $scope.varpartpub.loading = true;
  $scope.varpartpub.apps = true;
  $scope.varpartpub.noapp = false;
  $scope.varpartpub.error = null;

  $scope.varpartpub.appcount = null;
  $scope.varpartpub.limit = 100;
  $scope.varpartpub.skip = 0;
  $scope.varpartpub.pageappstatus = ['Partly Published', 'Partly Re-Published'];

  $scope.markaspub = function (app) {
    $scope.cs.showprogress = true;
    var inputParams = { id: LoopBackAuth.currentUserId, realm: app.id}

    if (app.appstatus == "Partly Published") {
      inputParams.status = "Published";
      inputParams.trialenddate = new Date();
    }
    else inputParams.status = "Re-Published"

    Appuser.prototype$c_updateAppStatus(inputParams,
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.publish.published");
      }, function (error) {
          $scope.varpartpub.error = "Error updating status. Try again later.";
          $scope.cs.showprogress = false;
      });
  };

  $scope.getAllApps($scope.varpartpub);


}]);
