angular.module('csapp').controller('unpublishedctrl', ['$scope', '$state', 'Appuser', 'LoopBackAuth', function ($scope, $state, Appuser, LoopBackAuth) {

  $scope.publish.currentnavitem = 'unpublished';
  $scope.varunpublished = {};

  $scope.varunpublished.loading = true;
  $scope.varunpublished.apps = true;
  $scope.varunpublished.noapp = false;
  $scope.varunpublished.error = null;

  $scope.varunpublished.appcount = null;
  $scope.varunpublished.limit = 100;
  $scope.varunpublished.skip = 0;
  $scope.varunpublished.pageappstatus = ['Un-Published'];

  $scope.submitreqtorepub = function (app) {
    $scope.cs.showprogress = true;
    Appuser.prototype$c_updateAppStatus({ id: LoopBackAuth.currentUserId, realm: app.id, status: 'Requested To Re-Publish'},
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.publish.reqtorepub");
      }, function (error) {
          $scope.varunpublished.error = "Error updating status. Try again later.";
          $scope.cs.showprogress = false;
      });
  }

  $scope.getAllApps($scope.varunpublished);

}]);
