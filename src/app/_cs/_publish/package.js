angular.module('csapp').controller('packagectrl', ['$scope', '$state', 'Appuser', 'LoopBackAuth', function ($scope, $state, Appuser, LoopBackAuth) {

  $scope.publish.currentnavitem = 'package';
  $scope.varpackage = {};

  $scope.varpackage.loading = true;
  $scope.varpackage.apps = true;
  $scope.varpackage.noapp = false;
  $scope.varpackage.error = null;

  $scope.varpackage.appcount = null;
  $scope.varpackage.limit = 100;
  $scope.varpackage.skip = 0;
  $scope.varpackage.pageappstatus = ['Packaging To Publish', 'Packaging To Re-Publish'];

  $scope.submittoappstores = function (app) {
    $scope.cs.showprogress = true;
    var changetostatus = null;
    if (app.appstatus == "Packaging To Publish") changetostatus = "Submitted To App Stores"
    else changetostatus = "Submitted To App Stores For Re-Publish"
    Appuser.prototype$c_updateAppStatus({ id: LoopBackAuth.currentUserId, realm: app.id, status: changetostatus},
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.publish.toappstores");
      }, function (error) {
          $scope.varpackage.error = "Error updating status. Try again later.";
          $scope.cs.showprogress = false;
      });
  }

  $scope.getAllApps($scope.varpackage);

}]);
