angular.module('csapp').controller('reqtounpubctrl', ['$scope', '$state', 'Appuser', 'LoopBackAuth', function ($scope, $state, Appuser, LoopBackAuth) {

  $scope.publish.currentnavitem = 'reqtounpub';
  $scope.varreqtounpub = {};

  $scope.varreqtounpub.loading = true;
  $scope.varreqtounpub.apps = true;
  $scope.varreqtounpub.noapp = false;
  $scope.varreqtounpub.error = null;

  $scope.varreqtounpub.appcount = null;
  $scope.varreqtounpub.limit = 100;
  $scope.varreqtounpub.skip = 0;
  $scope.varreqtounpub.pageappstatus = ['Requested To Un-Publish'];

  $scope.markaspub = function (app) {
    $scope.cs.showprogress = true;
    Appuser.prototype$c_updateAppStatus({ id: LoopBackAuth.currentUserId, realm: app.id, status: 'Published', autorenewalchange: 'Enable'},
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.publish.published");
      }, function (error) {
          $scope.varreqtounpub.error = "Error updating status. Try again later.";
          $scope.cs.showprogress = false;
      });
  }

  $scope.markasunpubinprogress = function (app) {
    $scope.cs.showprogress = true;
    Appuser.prototype$c_updateAppStatus({ id: LoopBackAuth.currentUserId, realm: app.id, status: 'Un-Publish In Progress'},
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.publish.unpublishinprogress");
      }, function (error) {
          $scope.varreqtounpub.error = "Error updating status. Try again later.";
          $scope.cs.showprogress = false;
      });
  }

  $scope.getAllApps($scope.varreqtounpub);

}]);
