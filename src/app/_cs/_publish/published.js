angular.module('csapp').controller('publishedctrl', ['$scope', '$state', 'Appuser', 'LoopBackAuth', function ($scope, $state, Appuser, LoopBackAuth) {

  $scope.publish.currentnavitem = 'published';
  $scope.varpublished = {};

  $scope.varpublished.loading = true;
  $scope.varpublished.apps = true;
  $scope.varpublished.noapp = false;
  $scope.varpublished.error = null;

  $scope.varpublished.appcount = null;
  $scope.varpublished.limit = 100;
  $scope.varpublished.skip = 0;
  $scope.varpublished.pageappstatus = ['Published', 'Re-Published'];

  $scope.submitreqtounpub = function (app) {
    $scope.cs.showprogress = true;
    Appuser.prototype$c_updateAppStatus({ id: LoopBackAuth.currentUserId, realm: app.id, status: 'Requested To Un-Publish', autorenewalchange: 'Disable'},
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.publish.reqtounpub");
      }, function (error) {
          $scope.varpublished.error = "Error updating status. Try again later.";
          $scope.cs.showprogress = false;
      });
  }

  $scope.submitreqtorepub = function (app) {
    $scope.cs.showprogress = true;
    Appuser.prototype$c_updateAppStatus({ id: LoopBackAuth.currentUserId, realm: app.id, status: 'Requested To Re-Publish'},
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.publish.reqtorepub");
      }, function (error) {
          $scope.varpublished.error = "Error updating status. Try again later.";
          $scope.cs.showprogress = false;
      });
  }

  $scope.getAllApps($scope.varpublished);

}]);
