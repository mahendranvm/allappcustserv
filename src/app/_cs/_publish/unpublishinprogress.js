angular.module('csapp').controller('unpublishinprogressctrl', ['$scope', '$state', 'Appuser', 'LoopBackAuth', function ($scope, $state, Appuser, LoopBackAuth) {

  $scope.publish.currentnavitem = 'unpublishinprogress';
  $scope.varunpublishinprogress = {};

  $scope.varunpublishinprogress.loading = true;
  $scope.varunpublishinprogress.apps = true;
  $scope.varunpublishinprogress.noapp = false;
  $scope.varunpublishinprogress.error = null;

  $scope.varunpublishinprogress.appcount = null;
  $scope.varunpublishinprogress.limit = 100;
  $scope.varunpublishinprogress.skip = 0;
  $scope.varunpublishinprogress.pageappstatus = ['Un-Publish In Progress'];

  $scope.markasunpub = function (app) {
    $scope.cs.showprogress = true;
    Appuser.prototype$c_updateAppStatus({ id: LoopBackAuth.currentUserId, realm: app.id, status: 'Un-Published'},
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.publish.unpublished");
      }, function (error) {
          $scope.varunpublishinprogress.error = "Error updating status. Try again later.";
          $scope.cs.showprogress = false;
      });
  }

  $scope.getAllApps($scope.varunpublishinprogress);

}]);
