angular.module('csapp').controller('notpublishedctrl', ['$scope', '$state', 'Appuser', 'LoopBackAuth', function ($scope, $state, Appuser, LoopBackAuth) {

  $scope.publish.currentnavitem = 'notpublished';
  $scope.varnotpublished = {};

  $scope.varnotpublished.loading = true;
  $scope.varnotpublished.apps = true;
  $scope.varnotpublished.noapp = false;
  $scope.varnotpublished.error = null;

  $scope.varnotpublished.appcount = null;
  $scope.varnotpublished.limit = 100;
  $scope.varnotpublished.skip = 0;
  $scope.varnotpublished.pageappstatus = ['Not Published'];

  $scope.submitreqtopub = function (app) {
    $scope.cs.showprogress = true;
    Appuser.prototype$c_updateAppStatus({ id: LoopBackAuth.currentUserId, realm: app.id, status: 'Requested To Publish'},
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.publish.reqtopub");
      }, function (error) {
          $scope.varnotpublished.error = "Error updating status. Try again later.";
          $scope.cs.showprogress = false;
      });
  }

  $scope.getAllApps($scope.varnotpublished);

}]);
