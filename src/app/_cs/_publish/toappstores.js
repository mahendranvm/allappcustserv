angular.module('csapp').controller('toappstoresctrl', ['$scope', '$state', 'Appuser', 'LoopBackAuth', '$mdDialog', function ($scope, $state, Appuser, LoopBackAuth, $mdDialog) {

  $scope.publish.currentnavitem = 'toappstores';
  $scope.vartoappstores = {};

  $scope.vartoappstores.loading = true;
  $scope.vartoappstores.apps = true;
  $scope.vartoappstores.noapp = false;
  $scope.vartoappstores.error = null;

  $scope.vartoappstores.appcount = null;
  $scope.vartoappstores.limit = 100;
  $scope.vartoappstores.skip = 0;
  $scope.vartoappstores.pageappstatus = ['Submitted To App Stores', 'Submitted To App Stores For Re-Publish'];

  $scope.markaspartpub = function (app) {
    $scope.cs.showprogress = true;
    if (app.appstatus == "Submitted To App Stores") changetostatus = "Partly Published"
    else changetostatus = "Partly Re-Published"
    Appuser.prototype$c_updateAppStatus({ id: LoopBackAuth.currentUserId, realm: app.id, status: changetostatus},
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.publish.partpub");
      }, function (error) {
          $scope.vartoappstores.error = "Error updating status. Try again later.";
          $scope.cs.showprogress = false;
      });
  }

  $scope.markaspub = function (app) {
    $scope.cs.showprogress = true;

    var inputParams = { id: LoopBackAuth.currentUserId, realm: app.id}

    if (app.appstatus == "Submitted To App Stores") {
      inputParams.status = "Published";
      inputParams.trialenddate = new Date();
    }
    else inputParams.status = "Re-Published"

    Appuser.prototype$c_updateAppStatus(inputParams,
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.publish.reqtopub");
      }, function (error) {
          $scope.vartoappstores.error = "Error updating status. Try again later.";
          $scope.cs.showprogress = false;
      });
  }


  $scope.getAllApps($scope.vartoappstores);



}]);
