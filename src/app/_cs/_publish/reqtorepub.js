angular.module('csapp').controller('reqtorepubctrl', ['$scope', '$state', 'Appuser', 'LoopBackAuth', function ($scope, $state, Appuser, LoopBackAuth) {

  $scope.publish.currentnavitem = 'reqtorepub';
  $scope.varreqtorepub = {};

  $scope.varreqtorepub.loading = true;
  $scope.varreqtorepub.apps = true;
  $scope.varreqtorepub.noapp = false;
  $scope.varreqtorepub.error = null;

  $scope.varreqtorepub.appcount = null;
  $scope.varreqtorepub.limit = 100;
  $scope.varreqtorepub.skip = 0;
  $scope.varreqtorepub.pageappstatus = ['Requested To Re-Publish'];

  $scope.submitpackage = function (app) {
    $scope.cs.showprogress = true;
    Appuser.prototype$c_updateAppStatus({ id: LoopBackAuth.currentUserId, realm: app.id, status: 'Packaging To Re-Publish'},
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.publish.package");
      }, function (error) {
          $scope.varreqtorepub.error = "Error updating status. Try again later.";
          $scope.cs.showprogress = false;
      });
  }

  $scope.getAllApps($scope.varreqtorepub);

}]);
