angular.module('csapp').controller('reqtopubctrl', ['$scope', '$state', 'Appuser', 'LoopBackAuth', function ($scope, $state, Appuser, LoopBackAuth) {

  $scope.publish.currentnavitem = 'reqtopub';
  $scope.varreqtopub = {};

  $scope.varreqtopub.loading = true;
  $scope.varreqtopub.apps = true;
  $scope.varreqtopub.noapp = false;
  $scope.varreqtopub.error = null;

  $scope.varreqtopub.appcount = null;
  $scope.varreqtopub.limit = 100;
  $scope.varreqtopub.skip = 0;
  $scope.varreqtopub.pageappstatus = ['Requested To Publish'];

  $scope.submitpackage = function (app) {
    $scope.cs.showprogress = true;
    Appuser.prototype$c_updateAppStatus({ id: LoopBackAuth.currentUserId, realm: app.id, status: 'Packaging To Publish'},
      function (response) {
          $scope.cs.showprogress = false;
          $state.go ("cs.publish.package");
      }, function (error) {
          $scope.varreqtopub.error = "Error updating status. Try again later.";
          $scope.cs.showprogress = false;
      });
  }

  $scope.getAllApps($scope.varreqtopub);

}]);
