angular.module('csapp').controller('newinteractionmessagectrl', ['$scope', '$state', '$stateParams', '$filter', 'Appuser', 'LoopBackAuth', 'interactmoduleservice', 'Upload', function ($scope, $state, $stateParams, $filter, Appuser, LoopBackAuth, interactmoduleservice, Upload) {

  $scope.adminappnewinteractionmessage = {};

  //overall variables
  $scope.adminappnewinteractionmessage.createmessageerror = null;
  $scope.adminappnewinteractionmessage.createbtndisabled = false;
  $scope.adminappnewinteractionmessage.selectedsectionid = null;
  $scope.adminappnewinteractionmessage.selectedsection = null;

  //message related
  $scope.adminappnewinteractionmessage.firstmessageformloading = true;
  $scope.adminappnewinteractionmessage.firstmessageformloadingerror = null;
  $scope.adminappnewinteractionmessage.nofirstmessageform = false;
  $scope.adminappnewinteractionmessage.firstmessageform = {};
  $scope.adminappnewinteractionmessage.interactiontitle = null;

  //user related
  $scope.adminappnewinteractionmessage.getusersloading = true;
  $scope.adminappnewinteractionmessage.getuserserror = null;
  $scope.adminappnewinteractionmessage.allusers = null;
  $scope.adminappnewinteractionmessage.nouser = false;
  $scope.adminappnewinteractionmessage.selectedUsers = [];
  $scope.adminappnewinteractionmessage.selectedUser = null;
  $scope.adminappnewinteractionmessage.searchUserText = null
  $scope.adminappnewinteractionmessage.broadcasttoall = false;
  $scope.adminappnewinteractionmessage.dontbroadcasttoall = true;

  //search user
  $scope.adminappnewinteractionmessage.searchTermUser = {};
  $scope.adminappnewinteractionmessage.searchTermUser.emailVerified = true;

  //tab controls
  $scope.adminappnewinteractionmessage.selectedIndex = 0;
  $scope.gotoselectuserstab = function () {
    $scope.adminappnewinteractionmessage.selectedIndex++;
  }
  $scope.gotoreviewandsendtab = function () {
    $scope.adminappnewinteractionmessage.selectedIndex++;
  }

  $scope.getRegisteredUsers = function () {
    //get all registered users
    $scope.cs.showprogress = true;
    Appuser.prototype$a_getRegisteredAppUsers({ id: LoopBackAuth.currentUserId, realm: "adminrealm"},
      function (response) {
          $scope.adminappnewinteractionmessage.allusers = response.success
          if (!$scope.adminappnewinteractionmessage.allusers || $scope.adminappnewinteractionmessage.allusers.length <=0) {
            $scope.adminappnewinteractionmessage.nouser = true;
          } else {
            $scope.adminappnewinteractionmessage.nouser = false;
            for (var i=0; i<$scope.adminappnewinteractionmessage.allusers.length; i++) {
              $scope.adminappnewinteractionmessage.allusers[i].fullname = $scope.adminappnewinteractionmessage.allusers[i].firstname + " " + $scope.adminappnewinteractionmessage.allusers[i].lastname
              $scope.adminappnewinteractionmessage.allusers[i].selecteduserind = false;
            }
          }
          $scope.cs.showprogress = false;
          $scope.adminappnewinteractionmessage.getusersloading = false;

          //now get message details
          //look at the section details, determine if first message should be a form
          if ($scope.adminappnewinteractionmessage.selectedsection.firstmessageformindicator) {
            $scope.adminappnewinteractionmessage.firstmessageform = $scope.adminappnewinteractionmessage.selectedsection.firstmessageform;
            //regular logic for appropriate display messages
            if (!$scope.adminappnewinteractionmessage.firstmessageform
               || !$scope.adminappnewinteractionmessage.firstmessageform.fields
               || $scope.adminappnewinteractionmessage.firstmessageform.fields.length <=0) {
              $scope.adminappnewinteractionmessage.nofirstmessageform = true;
            } else {
              $scope.adminappnewinteractionmessage.nofirstmessageform = false;
            }
            $scope.adminappnewinteractionmessage.firstmessageformloading = false;
          } else {
            $scope.adminappnewinteractionmessage.firstmessageformloading = false;
            $scope.adminappnewinteractionmessage.nofirstmessageform = false;
            $scope.adminappnewinteractionmessage.firstmessageform.fields = angular.copy(interactmoduleservice.standardmessage);
          }

      }, function (error) {
          //then check error object for custom error message, if none found show default message
          if (error && error.data && error.data.error && error.data.error.details && error.data.error.details.appErrMsg) {
            $scope.adminappnewinteractionmessage.getuserserror = error.data.error.details.appErrMsg;
          }
          else {
            $scope.adminappnewinteractionmessage.getuserserror = "Error: Could not get users. Please try again later.";
          }
          $scope.cs.showprogress = false;
          $scope.adminappnewinteractionmessage.getusersloading = false;
      });
  }


  //if a section id was pre selected and passed as a state param, then use it
  if ($stateParams.sectionid) {
    $scope.adminappnewinteractionmessage.selectedsectionid = $stateParams.sectionid;
    //when section details are resolved in parent abstract controller, determine the selected section here
    $scope.adminapp.selectedappPromise.$promise.then(function(){
      if (!$scope.adminappnewinteractionmessage.selectedsection) {
        for (var i=0; i<$scope.adminapp.selectedapp.apprealmsections.length; i++) {
          if ($scope.adminapp.selectedapp.apprealmsections[i].id==$stateParams.sectionid) {
            $scope.adminappnewinteractionmessage.selectedsection = $scope.adminapp.selectedapp.apprealmsections[i];
            break;
          }
        }
      }
      //initialize
      $scope.getRegisteredUsers();
    });
  }

  $scope.backtointeractions = function () {
    $state.go ("cs.interactions.interactionslist",{},{reload: true});
  }

  $scope.querySearchUsers = function (criteria) {
     return $filter('filter')($scope.adminappnewinteractionmessage.allusers, { fullname: criteria });
  }
  $scope.chipadded = function (user) {
    user.selecteduserind = true;
  }
  $scope.chipremoved = function (user) {
    user.selecteduserind = false;
  }
  $scope.listitemchanged = function (user) {
    if (user.selecteduserind) {$scope.adminappnewinteractionmessage.selectedUsers.push(user)}
    else {$scope.adminappnewinteractionmessage.selectedUsers.splice($scope.adminappnewinteractionmessage.selectedUsers.indexOf(user),1)}
  }

  //image related functions below $scope.adminappnewinteractionmessage.firstmessageform.fields
  $scope.beforeimagechange = function () {
    //called on select or drop, but before resizing or any other processing
    $scope.adminappnewinteractionmessage.firstmessageform.fields[1].showprogresscircle = true;
    $scope.adminappnewinteractionmessage.firstmessageform.fields[1].showimginfo = "Preparing...";
  }
  //resisizing the image
  $scope.afterimagechange = function () {
    //this will be called whenver there is a model change including when it is set to null, so react accordingly
    if ($scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldorigimg) {
      if ($scope.form1.file.$valid) {
        //get dimensions of file
        Upload.imageDimensions($scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldorigimg).then(function(dimensions){
          //if dimensions greater than 960*720, resize to that size and convert to jpeg for large image. else simply convert to jpeg.
          var largetowidth = null; var largetoheight = null;
          if (dimensions.width > 960 || dimensions.height > 720) {
            largetowidth = 960; largetoheight = 720;
          } else {
            largetowidth = dimensions.width; largetoheight = dimensions.height;
          }
          //now resize based on target sizes determined above
          // options: width, height, quality, type, ratio, centerCrop
          Upload.resize($scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldorigimg, {width: largetowidth, height: largetoheight, quality: 0.9, type: 'image/jpeg'})
          .then(function(resizedLargeFile){
            $scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldlargeimg = resizedLargeFile;
            $scope.adminappnewinteractionmessage.firstmessageform.fields[1].showprogresscircle = false;
            $scope.adminappnewinteractionmessage.firstmessageform.fields[1].showimginfo = null;
          });
        });
      } else {
        $scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldorigimg = null;
        $scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldlargeimg = null;
        $scope.adminappnewinteractionmessage.firstmessageform.fields[1].showprogresscircle = false;
        $scope.adminappnewinteractionmessage.firstmessageform.fields[1].showimginfo = "Something went wrong, unable to load image. Try again later!";
      }
    }
    else {
      $scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldorigimg = null;
      $scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldlargeimg = null;
      $scope.adminappnewinteractionmessage.firstmessageform.fields[1].showprogresscircle = false;
      $scope.adminappnewinteractionmessage.firstmessageform.fields[1].showimginfo = null;
    }
  }
  //resetting the image
  $scope.removeimage = function () {
    $scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldorigimg = null;
    $scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldlargeimg = null;
    $scope.adminappnewinteractionmessage.firstmessageform.fields[1].showprogresscircle = false;
    $scope.adminappnewinteractionmessage.firstmessageform.fields[1].showimginfo = null;
  }

  var createInteractionServerCall = function (interactioninput, fullfilekey) {
    Appuser.prototype$createInteraction(interactioninput,
      function (response) {
          $state.go("cs.interactions.interactionslist",
          {sectionid:$scope.adminappnewinteractionmessage.selectedsection.id}, {reload: true})
          $scope.cs.showprogress = false;
          $scope.adminappnewinteractionmessage.createbtndisabled = false;
      }, function (error) {
          //then check error object for custom error message, if none found show default message
          if (error && error.data && error.data.error && error.data.error.details && error.data.error.details.appErrMsg) {
            $scope.adminappnewinteractionmessage.createmessageerror = error.data.error.details.appErrMsg;
          }
          else {
            $scope.adminappnewinteractionmessage.createmessageerror = "Error: Could not create interaction. Please try again later.";
          }
          $scope.cs.showprogress = false;
          $scope.adminappnewinteractionmessage.createbtndisabled = false;
          //initiate code to delete the uploaded images from aws
          if (fullfilekey) {
            //remove pics
            Appuser.prototype$removePics({ id: LoopBackAuth.currentUserId, realm: "adminrealm", fullkeys: [{Key: fullfilekey}]},
            function (response) {}, function (error) {});
          }
      });
  }

  //create interaction
  $scope.createInteractionfn = function () {
    $scope.cs.showprogress = true;
    $scope.adminappnewinteractionmessage.createmessageerror = null;
    $scope.adminappnewinteractionmessage.createbtndisabled = true;

    var receiverids = [];
    var interactioninput = {};

    //get receiverids is not broadcasting
    if ($scope.adminappnewinteractionmessage.dontbroadcasttoall) {
      for (var i=0; i<$scope.adminappnewinteractionmessage.selectedUsers.length; i++) {
        receiverids.push($scope.adminappnewinteractionmessage.selectedUsers[i].id);
      }
    }

    //get first message form id if applicable
    if ($scope.adminappnewinteractionmessage.selectedsection.firstmessageformindicator) {
      interactioninput = {id: LoopBackAuth.currentUserId, realm: $scope.adminapp.selectedapp.id,
        sectionid: $scope.adminappnewinteractionmessage.selectedsection.id,
        title: $scope.adminappnewinteractionmessage.interactiontitle,
        broadcastind: $scope.adminappnewinteractionmessage.broadcasttoall, receiverids: receiverids,
        message: $scope.adminappnewinteractionmessage.firstmessageform.fields
      }
      createInteractionServerCall (interactioninput);
    } else {
      //simple message will always have a string field and optional image field, so add it to messagefields
      //we use this separate field since firstmessageform is a copy of standard message template that will have a placeholder for image
      var messagefields = [$scope.adminappnewinteractionmessage.firstmessageform.fields[0]];

      //if this is a simple message, logic to check for and upload image if present should be done before creating interaction in server
      if ($scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldlargeimg) {
        //create file name, adding timestamp + random number
        $scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldlargeimg.name = (new Date()).getTime() + (Math.floor(1000000 + Math.random() * 9000000)).toString() +".jpg";
        //form fullfilekey
        var fullfilekey = $scope.adminapp.selectedapp.id + "/" + $scope.adminappnewinteractionmessage.selectedsection.id + "/" +
                          LoopBackAuth.currentUserId + "/" + $scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldlargeimg.name;
        //get putobject signedurl from lb server
        Appuser.prototype$getPutSignedUrlForInteractionMessage({ id: LoopBackAuth.currentUserId, realm: $scope.adminapp.selectedapp.id,
          sectionid: $scope.adminappnewinteractionmessage.selectedsection.id, 
          fullkey: fullfilekey, imgtype: $scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldlargeimg.type},
        function (response) {
          //if response.success
          if (response.success) {
            //http upload and if successfully uploaded then call createMessageServerCall
            //on success, use url to upload photo
            Upload.http({
              url: response.success,
              data: $scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldlargeimg,
              method: 'PUT',
              headers : {'Content-Type': $scope.adminappnewinteractionmessage.firstmessageform.fields[1].fieldlargeimg.type}}).then(
              function (resp) {
                messagefields.push({ "fieldname": "Image",  "fieldtype": "Image",  "fieldrequired": false, "validvalues": [], "fieldvalue": fullfilekey});
                interactioninput = {id: LoopBackAuth.currentUserId, realm: $scope.adminapp.selectedapp.id,
                  sectionid: $scope.adminappnewinteractionmessage.selectedsection.id,
                  title: $scope.adminappnewinteractionmessage.interactiontitle,
                  broadcastind: $scope.adminappnewinteractionmessage.broadcasttoall, receiverids: receiverids,
                  message: messagefields
                }
                createInteractionServerCall (interactioninput,fullfilekey);
              }, function (error) {
                //unable to save
                $scope.cs.showprogress = false;
                $scope.adminappnewinteractionmessage.createbtndisabled = false;
                $scope.adminappnewinteractionmessage.createmessageerror = "Error: Failed to create, please try again later.";
              }, function (evt) {
              });
          } else {
            //unable to save
            $scope.cs.showprogress = false;
            $scope.adminappnewinteractionmessage.createbtndisabled = false;
            $scope.adminappnewinteractionmessage.createmessageerror = "Error: Failed to create, please try again later.";
          }
        }, function (error) {
          //unable to save
          $scope.cs.showprogress = false;
          $scope.adminappnewinteractionmessage.createbtndisabled = false;
          $scope.adminappnewinteractionmessage.createmessageerror = "Error: Failed to create, please try again later.";
        });
      } else {
        interactioninput = {id: LoopBackAuth.currentUserId, realm: $scope.adminapp.selectedapp.id,
          sectionid: $scope.adminappnewinteractionmessage.selectedsection.id,
          title: $scope.adminappnewinteractionmessage.interactiontitle,
          broadcastind: $scope.adminappnewinteractionmessage.broadcasttoall, receiverids: receiverids,
          message: messagefields
        }
        createInteractionServerCall (interactioninput);
      }
    }
  }

}]);
