angular.module('csapp').controller('interactionmessagesctrl', ['$scope', '$state', '$stateParams', 'Appuser', 'LoopBackAuth', '$mdDialog', function ($scope, $state, $stateParams, Appuser, LoopBackAuth, $mdDialog) {

  $scope.adminappviewinteractmessages = {}

  //interaction messages loading related variables
  $scope.adminappviewinteractmessages.messages = [];
  $scope.adminappviewinteractmessages.interaction = {};
  $scope.adminappviewinteractmessages.messagesloading = true;
  $scope.adminappviewinteractmessages.nomessages = false;
  $scope.adminappviewinteractmessages.messagesloadingerror = null;

  //variable used for pagination
  $scope.adminappviewinteractmessages.messagestotalcount = 0;
  $scope.adminappviewinteractmessages.messagesskip = 0;
  $scope.adminappviewinteractmessages.messageslimit = 50;
  $scope.adminappviewinteractmessages.messagesfrom = 0;
  $scope.adminappviewinteractmessages.messagesto = 0;
  $scope.adminappviewinteractmessages.messagesnextpage = false;
  $scope.adminappviewinteractmessages.messagesprevpage = false;

  //section variable
  $scope.adminappviewinteractmessages.selectedsection = null;

  //current user id
  $scope.adminappviewinteractmessages.currentUserId = LoopBackAuth.currentUserId;

  //delete interaction, message related variables
  $scope.adminappviewinteractmessages.removeinteractionerror = null;
  $scope.adminappviewinteractmessages.removinginteraction = false;
  $scope.adminappviewinteractmessages.removeinteractionmessageerror = null;
  $scope.adminappviewinteractmessages.removinginteractionmessage = false;

  //get section info - this function is a wrapper for a promise callback and will be called in the success callback of nextmessagespage on initial load
  $scope.getselectedsectioninfo = function () {
    //when section details are resolved in parent abstract controller, determine the selected section here
    $scope.adminapp.selectedappPromise.$promise.then(function(){
      if (!$scope.adminappviewinteractmessages.selectedsection) {
        for (var i=0; i<$scope.adminapp.selectedapp.apprealmsections.length; i++) {
          if ($scope.adminapp.selectedapp.apprealmsections[i].id==$scope.adminappviewinteractmessages.interaction.sectionid) {
            $scope.adminappviewinteractmessages.selectedsection = $scope.adminapp.selectedapp.apprealmsections[i];
            break;
          }
        }
      }
    });
  }

  //get interaction messages for the selected interaction
  $scope.nextmessagespage = function () {
    $scope.adminappviewinteractmessages.messagesloading = true;
    $scope.adminappviewinteractmessages.messagesloadingerror = null;
    $scope.adminappviewinteractmessages.nomessages = false;

    $scope.cs.showprogress = true;
    Appuser.prototype$getInteractionMessages({ id: LoopBackAuth.currentUserId, realm: "adminrealm",
      sectionid: $stateParams.sectionid, interactionid: $stateParams.interactionid,
      limit: $scope.adminappviewinteractmessages.messageslimit,
      skip:$scope.adminappviewinteractmessages.messagesto},
      function (response) {
          //pagination logic
          if (response.success.totalcount) {
            $scope.adminappviewinteractmessages.messagestotalcount = response.success.totalcount;
            $scope.adminappviewinteractmessages.messagesfrom = $scope.adminappviewinteractmessages.messagesto + 1;
            //logic for > first page pagination control values
            if ($scope.adminappviewinteractmessages.messagestotalcount <= $scope.adminappviewinteractmessages.messageslimit + $scope.adminappviewinteractmessages.messagesto) {
              $scope.adminappviewinteractmessages.messagesto = $scope.adminappviewinteractmessages.messagestotalcount;
              $scope.adminappviewinteractmessages.messagesnextpage = false;
            } else {
              $scope.adminappviewinteractmessages.messagesto = $scope.adminappviewinteractmessages.messageslimit + $scope.adminappviewinteractmessages.messagesto;
              $scope.adminappviewinteractmessages.messagesnextpage = true;
            }
            if ($scope.adminappviewinteractmessages.messagesfrom > 1) {$scope.adminappviewinteractmessages.messagesprevpage = true;}
            else {$scope.adminappviewinteractmessages.messagesprevpage = false;}
          }
          //final check to handle deletes resulting in from > to
          if ($scope.adminappviewinteractmessages.messagesfrom > $scope.adminappviewinteractmessages.messagesto) {
            $scope.adminappviewinteractmessages.messagesloadingerror = "Error: Could not load messages properly. Please refresh the page or try again later.";
          }
          //regular logic for appropriate display messages
          if (response.success && response.success.totalcount && response.success.totalcount>0) {
            $scope.adminappviewinteractmessages.messages = response.success.interactionmessages;
            $scope.adminappviewinteractmessages.interaction = response.success.interaction;
            $scope.getnamesandunreadcount([$scope.adminappviewinteractmessages.interaction]);
            $scope.markasread();
            $scope.getselectedsectioninfo()
          } else {
            $scope.adminappviewinteractmessages.nomessages = true;
          }
          $scope.cs.showprogress = false;
          $scope.adminappviewinteractmessages.messagesloading = false;
      }, function (error) {
          $scope.adminappviewinteractmessages.messagesloadingerror = "Error: Could not load Interaction Messages. Please try again later.";
          $scope.cs.showprogress = false;
          $scope.adminappviewinteractmessages.messagesloading = false;
      });
  }

  //get interaction messagess for the selected interaction
  $scope.prevmessagespage = function () {
    $scope.adminappviewinteractmessages.messagesloading = true;
    $scope.adminappviewinteractmessages.messagesloadingerror = null;
    $scope.adminappviewinteractmessages.nomessages = false;

    $scope.cs.showprogress = true;
    Appuser.prototype$getInteractionMessages({ id: LoopBackAuth.currentUserId, realm: "adminrealm",
      sectionid: $stateParams.sectionid, interactionid: $stateParams.interactionid,
      limit: $scope.adminappviewinteractmessages.messageslimit,
      skip:($scope.adminappviewinteractmessages.messagesfrom-1-$scope.adminappviewinteractmessages.messageslimit)},
      function (response) {
          //pagination logic
          if (response.success.totalcount) {
            $scope.adminappviewinteractmessages.messagestotalcount = response.success.totalcount;
            $scope.adminappviewinteractmessages.messagesfrom = $scope.adminappviewinteractmessages.messagesfrom-$scope.adminappviewinteractmessages.messageslimit;
            $scope.adminappviewinteractmessages.messagesto = $scope.adminappviewinteractmessages.messagesfrom-1+$scope.adminappviewinteractmessages.messageslimit;
            //logic for > first page pagination control values
            if ($scope.adminappviewinteractmessages.messagestotalcount <= $scope.adminappviewinteractmessages.messagesfrom-1+$scope.adminappviewinteractmessages.messageslimit) {
              $scope.adminappviewinteractmessages.messagesto = $scope.adminappviewinteractmessages.messagestotalcount;
              $scope.adminappviewinteractmessages.messagesnextpage = false;
            } else {
              $scope.adminappviewinteractmessages.messagesto = $scope.adminappviewinteractmessages.messagesfrom-1+$scope.adminappviewinteractmessages.messageslimit;
              $scope.adminappviewinteractmessages.messagesnextpage = true;
            }
            if ($scope.adminappviewinteractmessages.messagesfrom > 1) {$scope.adminappviewinteractmessages.messagesprevpage = true;}
            else {$scope.adminappviewinteractmessages.messagesprevpage = false;}
          }
          //final check to handle deletes resulting in from > to
          if ($scope.adminappviewinteractmessages.messagesfrom > $scope.adminappviewinteractmessages.messagesto) {
            $scope.adminappviewinteractmessages.messagesloadingerror = "Error: Could not load messages properly. Please refresh the page or try again later.";
          }
          //regular logic for appropriate display messages
          if (response.success && response.success.totalcount && response.success.totalcount>0) {
            $scope.adminappviewinteractmessages.messages = response.success.interactionmessages;
            $scope.adminappviewinteractmessages.interaction = response.success.interaction;
            $scope.getnamesandunreadcount([$scope.adminappviewinteractmessages.interaction]);
            $scope.markasread();
          } else {
            $scope.adminappviewinteractmessages.nomessages = true;
          }
          $scope.cs.showprogress = false;
          $scope.adminappviewinteractmessages.messagesloading = false;
      }, function (error) {
          $scope.adminappviewinteractmessages.messagesloadingerror = "Error: Could not load Interaction Messages. Please try again later.";
          $scope.cs.showprogress = false;
          $scope.adminappviewinteractmessages.messagesloading = false;
      });
  }

  //initialize
  $scope.nextmessagespage();

  //reset the unreadcount info in server
  $scope.markasread = function () {
    if ($scope.adminappviewinteractmessages.interaction.myunreadcount >0) {

      var lastreadmessagedate =  new Date('1/1/2000'); //dummy date
      var updateonbookmark =  new Date('1/1/2000'); //dummy date

      if ($scope.adminappviewinteractmessages.interaction.interactionusertrackers && $scope.adminappviewinteractmessages.interaction.interactionusertrackers[0]) updateonbookmark = $scope.adminappviewinteractmessages.interaction.interactionusertrackers[0].updatedon;
      if ($scope.adminappviewinteractmessages.messages[0] && $scope.adminappviewinteractmessages.messages[0].createdon) lastreadmessagedate = $scope.adminappviewinteractmessages.messages[0].createdon;

      Appuser.prototype$updateInteractionUserTracker({ id: LoopBackAuth.currentUserId, realm: "adminrealm",
        interactionid: $scope.adminappviewinteractmessages.interaction.id,
        sectionid: $scope.adminappviewinteractmessages.interaction.sectionid,
        reduceby: $scope.adminappviewinteractmessages.interaction.myunreadcount,
        lastreadmessagedate: lastreadmessagedate, updateonbookmark: updateonbookmark},
        function (response) {
            //do nothing
            console.log("Unread Count successfully reset.");
        }, function (error) {
            //do nothing
            console.log(error);
        });
    }
  }

  //delete interaction
  $scope.deleteinteraction = function (ev, title, content) {
    // confirm dialog
    var confirm = $mdDialog.confirm()
          .title(title || 'This will permanently delete this interaction and all posted messages.')
          .textContent(content || 'Do you want to continue?')
          .targetEvent(ev)
          .ok('Yes')
          .cancel('No');
    $mdDialog.show(confirm).then(function() {
      $scope.cs.showprogress = true;
      $scope.adminappviewinteractmessages.removeinteractionerror = null;
      $scope.adminappviewinteractmessages.removinginteraction = true;

      Appuser.prototype$deleteInteraction({id: LoopBackAuth.currentUserId, realm: $scope.adminapp.selectedapp.id,
        sectionid: $scope.adminappviewinteractmessages.interaction.sectionid, interactionid: $stateParams.interactionid},
        function (response) {
            $state.go ("cs.interactions.interactionslist",{},{reload: true});
            $scope.adminappviewinteractmessages.removinginteraction = false;
            $scope.cs.showprogress = false;
        }, function (error) {
            //then check error object for custom error message, if none found show default message
            if (error && error.data && error.data.error && error.data.error.details && error.data.error.details.appErrMsg) {
              $scope.adminappviewinteractmessages.removeinteractionerror = error.data.error.details.appErrMsg;
            }
            else {
              $scope.adminappviewinteractmessages.removeinteractionerror = "Error: Failed to delete. Please try again later.";
            }
            $scope.adminappviewinteractmessages.removinginteraction = false;
            $scope.cs.showprogress = false;
        });
    }, function() {
    });
  }

  //delete interaction message
  $scope.deleteinteractionmessage = function (ev, interactionmessage) {
    if ($scope.adminappviewinteractmessages.messagestotalcount == 1 &&
      !($scope.adminappviewinteractmessages.selectedsection.admincandeleteanyinteraction ||
      ($scope.adminappviewinteractmessages.selectedsection.admincandeleteowninteraction &&
        $scope.adminappviewinteractmessages.selectedsection.initiatedby=='Administrator'))) {
          $mdDialog.show(
            $mdDialog.alert()
              .clickOutsideToClose(true)
              .title('Cannot delete last message.')
              .textContent('This is the only message posted. Deleting this requires persmission to delete Interaction.')
              .ok('Ok')
              .targetEvent(ev)
          );
      } else if ($scope.adminappviewinteractmessages.messagestotalcount == 1) {
        $scope.deleteinteraction (ev, "This is the only message posted. Deleting this will permanently delete this entire Interaction.");
      } else {
        // confirm dialog
        var confirm = $mdDialog.confirm()
              .title('This will permanently delete this message.')
              .textContent('Do you want to continue?')
              .targetEvent(ev)
              .ok('Yes')
              .cancel('No');
        $mdDialog.show(confirm).then(function() {
          $scope.cs.showprogress = true;
          $scope.adminappviewinteractmessages.removeinteractionmessageerror = null;
          $scope.adminappviewinteractmessages.removinginteractionmessage = true;

          Appuser.prototype$deleteInteractionMessage({id: LoopBackAuth.currentUserId, realm: $scope.adminapp.selectedapp.id,
            sectionid: $scope.adminappviewinteractmessages.interaction.sectionid, interactionid: $stateParams.interactionid,
            interactionmessageid: interactionmessage.id},
            function (response) {
                $state.go ("cs.interactions.interactionmessages.messageslist",{interactionid:$stateParams.interactionid},{reload: true});
                $scope.adminappviewinteractmessages.removinginteractionmessage = false;
                $scope.cs.showprogress = false;
            }, function (error) {
                //then check error object for custom error message, if none found show default message
                if (error && error.data && error.data.error && error.data.error.details && error.data.error.details.appErrMsg) {
                  $scope.adminappviewinteractmessages.removeinteractionmessageerror = error.data.error.details.appErrMsg;
                }
                else {
                  $scope.adminappviewinteractmessages.removeinteractionmessageerror = "Error: Failed to delete. Please try again later.";
                }
                $scope.adminappviewinteractmessages.removinginteractionmessage = false;
                $scope.cs.showprogress = false;
            });
        }, function() {
        });
      }
  }

}]);
