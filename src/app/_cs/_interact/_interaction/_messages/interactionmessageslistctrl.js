angular.module('csapp').controller('interactionmessageslistctrl', ['$scope', '$state', '$stateParams', 'Appuser', 'LoopBackAuth', 'interactmoduleservice', '$mdDialog', '$mdMedia', 'Upload', function ($scope, $state, $stateParams, Appuser, LoopBackAuth, interactmoduleservice, $mdDialog, $mdMedia, Upload) {

  $scope.adminappviewinteractmessageslist = {};
  $scope.adminappviewinteractmessageslist.shownew = false;
  $scope.adminappviewinteractmessageslist.messageforms = [];
  $scope.adminappviewinteractmessageslist.messageformsdownloaded = false;
  $scope.adminappviewinteractmessageslist.messageformsloading = true;
  $scope.adminappviewinteractmessageslist.messageformsloadingerror = null;
  $scope.adminappviewinteractmessageslist.nomessageforms = false;

  //new message related variables
  $scope.adminappviewinteractmessageslist.newmessageform = {};
  $scope.adminappviewinteractmessageslist.newmessageform.fields = angular.copy(interactmoduleservice.standardmessage);


  $scope.backtointeractions = function () {
    $state.go ("cs.interactions.interactionslist",{},{reload: true});
  }

  $scope.shownewmessageform = function () {
    if ($scope.adminappviewinteractmessageslist.shownew) {
      $scope.adminappviewinteractmessageslist.shownew = false;
      //reset the message
      $scope.adminappviewinteractmessageslist.newmessageform = {};
      $scope.adminappviewinteractmessageslist.newmessageform.fields = angular.copy(interactmoduleservice.standardmessage);
    } else {
      $scope.adminappviewinteractmessageslist.shownew = true;
    }
  }

  $scope.beforeimagechange = function () {
    //called on select or drop, but before resizing or any other processing
    $scope.adminappviewinteractmessageslist.newmessageform.fields[1].showprogresscircle = true;
    $scope.adminappviewinteractmessageslist.newmessageform.fields[1].showimginfo = "Preparing...";
  }

  $scope.afterimagechange = function () {
    //this will be called whenver there is a model change including when it is set to null, so react accordingly
    if ($scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldorigimg) {
      if ($scope.newmsgform.file.$valid) {
        //get dimensions of file
        Upload.imageDimensions($scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldorigimg).then(function(dimensions){
          //if dimensions greater than 960*720, resize to that size and convert to jpeg for large image. else simply convert to jpeg.
          var largetowidth = null; var largetoheight = null;
          if (dimensions.width > 960 || dimensions.height > 720) {
            largetowidth = 960; largetoheight = 720;
          } else {
            largetowidth = dimensions.width; largetoheight = dimensions.height;
          }
          //now resize based on target sizes determined above
          // options: width, height, quality, type, ratio, centerCrop
          Upload.resize($scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldorigimg, {width: largetowidth, height: largetoheight, quality: 0.9, type: 'image/jpeg'})
          .then(function(resizedLargeFile){
            $scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldlargeimg = resizedLargeFile;
            $scope.adminappviewinteractmessageslist.newmessageform.fields[1].showprogresscircle = false;
            $scope.adminappviewinteractmessageslist.newmessageform.fields[1].showimginfo = null;
          });
        });
      } else {
        $scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldorigimg = null;
        $scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldlargeimg = null;
        $scope.adminappviewinteractmessageslist.newmessageform.fields[1].showprogresscircle = false;
        $scope.adminappviewinteractmessageslist.newmessageform.fields[1].showimginfo = "Something went wrong, unable to load image. Try again later!";
      }
    }
    else {
      $scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldorigimg = null;
      $scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldlargeimg = null;
      $scope.adminappviewinteractmessageslist.newmessageform.fields[1].showprogresscircle = false;
      $scope.adminappviewinteractmessageslist.newmessageform.fields[1].showimginfo = null;
    }
  }

  $scope.removeimage = function () {
    $scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldorigimg = null;
    $scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldlargeimg = null;
    $scope.adminappviewinteractmessageslist.newmessageform.fields[1].showprogresscircle = false;
    $scope.adminappviewinteractmessageslist.newmessageform.fields[1].showimginfo = null;
  }

  var createMessageServerCall = function (interactionmsginput, fullfilekey) {
    Appuser.prototype$createInteractionMessage(interactionmsginput,
      function (response) {
          $state.go("cs.interactions.interactionmessages.messageslist",
          {interactionid:$scope.adminappviewinteractmessages.interaction.id}, {reload: true})
          $scope.cs.showprogress = false;
          $scope.adminappviewinteractmessageslist.createbtndisabled = false;
      }, function (error) {
          //then check error object for custom error message, if none found show default message
          if (error && error.data && error.data.error && error.data.error.details && error.data.error.details.appErrMsg) {
            $scope.adminappviewinteractmessageslist.createmessageerror = error.data.error.details.appErrMsg;
          }
          else {
            $scope.adminappviewinteractmessageslist.createmessageerror = "Error: Could not create message. Please try again later.";
          }
          $scope.cs.showprogress = false;
          $scope.adminappviewinteractmessageslist.createbtndisabled = false;
          //initiate code to delete the uploaded images from aws
          if (fullfilekey) {
            //remove pics
            Appuser.prototype$removePics({ id: LoopBackAuth.currentUserId, realm: "adminrealm", fullkeys: [{Key: fullfilekey}]},
            function (response) {}, function (error) {});
          }
      });
  }

  $scope.createnewmessage = function () {
    $scope.cs.showprogress = true;
    $scope.adminappviewinteractmessageslist.createmessageerror = null;
    $scope.adminappviewinteractmessageslist.createbtndisabled = true;

    //initialize variables to be given as input for create message function
    var interactionmsginput = {};

    //simple message will always have a string field and optional image field
    var messagefields = [$scope.adminappviewinteractmessageslist.newmessageform.fields[0]];
    //if simple message and if image is present
    if ($scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldlargeimg) {
      //create file name, adding timestamp, random number with last 10 chars of file name
      $scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldlargeimg.name = (new Date()).getTime() + (Math.floor(1000000 + Math.random() * 9000000)).toString() +".jpg";
      //form fullfilekey
      var fullfilekey = "adminrealm" + "/" + $scope.adminappviewinteractmessages.interaction.sectionid + "/" +
                        $scope.adminappviewinteractmessages.interaction.id + "/" + LoopBackAuth.currentUserId + "/" +
                        $scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldlargeimg.name;
      //get putobject signedurl from lb server
      Appuser.prototype$getPutSignedUrlForInteractionMessage({ id: LoopBackAuth.currentUserId, realm: "adminrealm",
        sectionid: $scope.adminappviewinteractmessages.interaction.sectionid, interactionid: $scope.adminappviewinteractmessages.interaction.id,
        fullkey: fullfilekey, imgtype: $scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldlargeimg.type},
      function (response) {
        //if response.success
        if (response.success) {
          //http upload and if successfully uploaded then call createMessageServerCall
          //on success, use url to upload photo
          Upload.http({
            url: response.success,
            data: $scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldlargeimg,
            method: 'PUT',
            headers : {'Content-Type': $scope.adminappviewinteractmessageslist.newmessageform.fields[1].fieldlargeimg.type}}).then(
            function (resp) {
              messagefields.push({ "fieldname": "Image",  "fieldtype": "Image",  "fieldrequired": false, "validvalues": [], "fieldvalue": fullfilekey});
              interactionmsginput = {id: LoopBackAuth.currentUserId, realm: $scope.adminapp.selectedapp.id,
                sectionid: $scope.adminappviewinteractmessages.interaction.sectionid,
                interactionid: $scope.adminappviewinteractmessages.interaction.id, message: messagefields
              }
              createMessageServerCall (interactionmsginput,fullfilekey);
            }, function (error) {
              //unable to save
              $scope.cs.showprogress = false;
              $scope.adminappviewinteractmessageslist.createbtndisabled = false;
              $scope.adminappviewinteractmessageslist.createmessageerror = "Error: Could not create message. Please try again later.";
            }, function (evt) {

            });
        } else {
          //unable to save
          $scope.cs.showprogress = false;
          $scope.adminappviewinteractmessageslist.createbtndisabled = false;
          $scope.adminappviewinteractmessageslist.createmessageerror = "Error: Could not create message. Please try again later.";
        }
      }, function (error) {
        //unable to save
        $scope.cs.showprogress = false;
        $scope.adminappviewinteractmessageslist.createbtndisabled = false;
        $scope.adminappviewinteractmessageslist.createmessageerror = "Error: Could not create message. Please try again later.";
      });
    } else {
      //if no image is present then form interaction message accordingly and call createMessageServerCall
      interactionmsginput = {id: LoopBackAuth.currentUserId, realm: $scope.adminapp.selectedapp.id,
        sectionid: $scope.adminappviewinteractmessages.interaction.sectionid,
        interactionid: $scope.adminappviewinteractmessages.interaction.id, message: messagefields
      }
      createMessageServerCall (interactionmsginput);
    }
  };

  $scope.openphoto = function (interactionmessage, event) {
    $mdDialog.show({
      controller: expandedPicCtrl,
      templateUrl: 'app/_cs/_interact/_interaction/_messages/expandedPic.html',
      locals: {interactionmessage: interactionmessage},
      parent: angular.element(document.body),
      targetEvent: event,
      clickOutsideToClose:true,
      fullscreen: true
    }). then(function() {
    }, function() {
    });
  }

  $scope.previewDeleteInteractionMessage = function (ev, message) {
   $mdDialog.show(
     $mdDialog.alert()
       .clickOutsideToClose(true)
       .title('This is just a Preview.')
       .textContent('If you would like to actually delete this message now, do so from the panel on the left. This is to demonstrate that when the app user tries to delete a message created by the user, an alert message will be shown to the app users to confirm the action. On affirmation, the message will be deleted. ')
       .ok('Ok')
       .targetEvent(ev)
   );
  }

}]);

function expandedPicCtrl($scope, $mdDialog, interactionmessage) {
  $scope.interactionmessage = interactionmessage;
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
}
