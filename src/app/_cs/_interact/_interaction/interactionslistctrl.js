angular.module('csapp').controller('interactionslistctrl', ['$scope', '$state', '$stateParams', 'base64images', function ($scope, $state, $stateParams, base64images) {

  $scope.formats = {
    sameDay: 'h:mm a',
    lastDay: 'ddd',
    lastWeek: 'ddd',
    sameElse: 'DD/MM/YYYY'
  }

  $scope.base64images = base64images;


  $scope.gotoupdateinteractmessageform = function () {
    $state.go ("cs.updateinteractmessageform", {sectionid: $stateParams.sectionid})
  }

}]);
