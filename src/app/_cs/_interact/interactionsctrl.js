angular.module('csapp').controller('interactionsctrl', ['$scope', '$state', '$stateParams', 'Appuser', 'LoopBackAuth', '$mdDialog', 'csconfig', function ($scope, $state, $stateParams, Appuser, LoopBackAuth, $mdDialog, csconfig) {
/*
  1. get latest 25 interactions, along with total interaction count, total unread interaction count, for all sections or a given section if section id is present
  2. display the list with info on remaining interaction and remaining unread interaction at bottom, with "show" - on click fetch the next set
  3. on click of an interaction, open the interactiondetail state (similar fetch last XX messages logic there)
*/

  $scope.adminappviewinteractsection = {}
  //section related variables
  $scope.adminappviewinteractsection.selectedsectionid = null;
  $scope.adminappviewinteractsection.searchTermSection = {}
  $scope.adminappviewinteractsection.searchTermSection.module = "Interact";
  $scope.adminappviewinteractsection.selectedsection = null;

  //interaction loading related variables
  $scope.adminappviewinteractsection.interactions = [];
  $scope.adminappviewinteractsection.selectedinteraction = null;
  $scope.adminappviewinteractsection.interactionsloading = true;
  $scope.adminappviewinteractsection.nointeraction = false;
  $scope.adminappviewinteractsection.interactionsloadingerror = null;

  //variable used for pagination
  $scope.adminappviewinteractsection.interactionstotalcount = 0;
  $scope.adminappviewinteractsection.interactionslimit = 25;
  $scope.adminappviewinteractsection.interactionsfrom = 0;
  $scope.adminappviewinteractsection.interactionsto = 0;
  $scope.adminappviewinteractsection.interactionsnextpage = false;
  $scope.adminappviewinteractsection.interactionsprevpage = false;

  //if a section id was pre selected and passed as a state param, then use it
  if ($stateParams.sectionid) {
    $scope.adminappviewinteractsection.selectedsectionid = $stateParams.sectionid;
    //when section details are resolved in parent abstract controller, determine the selected section here
    $scope.adminapp.selectedappPromise.$promise.then(function(){
      if (!$scope.adminappviewinteractsection.selectedsection) {
        for (var i=0; i<$scope.adminapp.selectedapp.apprealmsections.length; i++) {
          if ($scope.adminapp.selectedapp.apprealmsections[i].id==$stateParams.sectionid) {
            $scope.adminappviewinteractsection.selectedsection = $scope.adminapp.selectedapp.apprealmsections[i];
            break;
          }
        }
      }
      //initialize
      $scope.nextpage();
    });

    if ($stateParams.sectionid == "adminrealmsupport") csconfig.mode = "support";
    if ($stateParams.sectionid == "adminrealmnotifications") csconfig.mode = "notifications";
  }


  $scope.getnamesandunreadcount = function (interactionsarray) {
    //logic to get unread count and audience list
    for (var i=0; i<interactionsarray.length; i++) {
      //get unreadcount
      if (interactionsarray[i].interactionusertrackers
         && interactionsarray[i].interactionusertrackers.length == 1) {
          interactionsarray[i].myunreadcount = interactionsarray[i].interactionusertrackers[0].unreadcount;
        } else {
          //Message count is same as unread if no tracker record exists
          interactionsarray[i].myunreadcount = interactionsarray[i].messagecount;
        }
      //get receiver names
      if (interactionsarray[i].broadcastind) {
        interactionsarray[i].displayrecievernames = "All Users"
      } else {
        if (interactionsarray[i].receivingusers && interactionsarray[i].receivingusers.length>0) {
          interactionsarray[i].displayrecievernames = interactionsarray[i].receivingusers[0].firstname + " " + interactionsarray[i].receivingusers[0].lastname
          for (var j=1; j<interactionsarray[i].receivingusers.length; j++) {
            interactionsarray[i].displayrecievernames = interactionsarray[i].displayrecievernames
             + ", " + interactionsarray[i].receivingusers[j].firstname + " " + interactionsarray[i].receivingusers[j].lastname
          }
        }
      }
    }
  }

  //get interactions for the selected section (or all sections if no section id is available)
  $scope.nextpage = function () {
    $scope.adminappviewinteractsection.interactionsloading = true;
    $scope.adminappviewinteractsection.interactionsloadingerror = null;
    $scope.adminappviewinteractsection.nointeraction = false;

    var getInteractionsparams = { id: LoopBackAuth.currentUserId, realm: "adminrealm",
      sectionid: $scope.adminappviewinteractsection.selectedsectionid,
      limit: $scope.adminappviewinteractsection.interactionslimit,
      skip:$scope.adminappviewinteractsection.interactionsto}

    $scope.cs.showprogress = true;
    Appuser.prototype$getInteractions(getInteractionsparams,
      function (response) {
          //pagination logic
          if (response.success.totalcount) {
            $scope.adminappviewinteractsection.interactionstotalcount = response.success.totalcount;
            $scope.adminappviewinteractsection.interactionsfrom = $scope.adminappviewinteractsection.interactionsto + 1;
            //logic for > first page pagination control values
            if ($scope.adminappviewinteractsection.interactionstotalcount <= $scope.adminappviewinteractsection.interactionslimit + $scope.adminappviewinteractsection.interactionsto) {
              $scope.adminappviewinteractsection.interactionsto = $scope.adminappviewinteractsection.interactionstotalcount;
              $scope.adminappviewinteractsection.interactionsnextpage = false;
            } else {
              $scope.adminappviewinteractsection.interactionsto = $scope.adminappviewinteractsection.interactionslimit + $scope.adminappviewinteractsection.interactionsto;
              $scope.adminappviewinteractsection.interactionsnextpage = true;
            }
            if ($scope.adminappviewinteractsection.interactionsfrom > 1) {$scope.adminappviewinteractsection.interactionsprevpage = true;}
            else {$scope.adminappviewinteractsection.interactionsprevpage = false;}
          }
          //final check to handle deletes resulting in from > to
          if ($scope.adminappviewinteractsection.interactionsfrom > $scope.adminappviewinteractsection.interactionsto) {
            $scope.adminappviewinteractsection.interactionsloadingerror = "Error: Could not load interactions properly. Please refresh the page or try again later.";
          }
          //regular logic for appropriate display messages
          if (response.success && response.success.totalcount && response.success.totalcount>0 && response.success.interactions && response.success.interactions.length) {
            $scope.adminappviewinteractsection.interactions = response.success.interactions;
            $scope.getnamesandunreadcount($scope.adminappviewinteractsection.interactions);
          } else {
            $scope.adminappviewinteractsection.nointeraction = true;
          }
          $scope.cs.showprogress = false;
          $scope.adminappviewinteractsection.interactionsloading = false;
      }, function (error) {
          $scope.adminappviewinteractsection.interactionsloadingerror = "Error: Could not load Interactions. Please try again later.";
          $scope.cs.showprogress = false;
          $scope.adminappviewinteractsection.interactionsloading = false;
      });
  }

  //get interactions for the selected section (or all sections if no section id is available)
  $scope.prevpage = function () {
    $scope.adminappviewinteractsection.interactionsloading = true;
    $scope.adminappviewinteractsection.interactionsloadingerror = null;
    $scope.adminappviewinteractsection.nointeraction = false;

    //validate section id
    var getInteractionsparams = {};
    getInteractionsparams = { id: LoopBackAuth.currentUserId, realm: "adminrealm",
      sectionid: $scope.adminappviewinteractsection.selectedsectionid,
      limit: $scope.adminappviewinteractsection.interactionslimit,
      skip:($scope.adminappviewinteractsection.interactionsfrom-1-$scope.adminappviewinteractsection.interactionslimit)}

    $scope.cs.showprogress = true;
    Appuser.prototype$getInteractions(getInteractionsparams,
      function (response) {
          //pagination logic
          if (response.success.totalcount) {
            $scope.adminappviewinteractsection.interactionstotalcount = response.success.totalcount;
            $scope.adminappviewinteractsection.interactionsfrom = $scope.adminappviewinteractsection.interactionsfrom-$scope.adminappviewinteractsection.interactionslimit;
            $scope.adminappviewinteractsection.interactionsto = $scope.adminappviewinteractsection.interactionsfrom-1+$scope.adminappviewinteractsection.interactionslimit;
            //logic for > first page pagination control values
            if ($scope.adminappviewinteractsection.interactionstotalcount <= $scope.adminappviewinteractsection.interactionsfrom-1+$scope.adminappviewinteractsection.interactionslimit) {
              $scope.adminappviewinteractsection.interactionsto = $scope.adminappviewinteractsection.interactionstotalcount;
              $scope.adminappviewinteractsection.interactionsnextpage = false;
            } else {
              $scope.adminappviewinteractsection.interactionsto = $scope.adminappviewinteractsection.interactionsfrom-1+$scope.adminappviewinteractsection.interactionslimit;
              $scope.adminappviewinteractsection.interactionsnextpage = true;
            }
            if ($scope.adminappviewinteractsection.interactionsfrom > 1) {$scope.adminappviewinteractsection.interactionsprevpage = true;}
            else {$scope.adminappviewinteractsection.interactionsprevpage = false;}
          }
          //final check to handle deletes resulting in from > to
          if ($scope.adminappviewinteractsection.interactionsfrom > $scope.adminappviewinteractsection.interactionsto) {
            $scope.adminappviewinteractsection.interactionsloadingerror = "Error: Could not load interactions properly. Please refresh the page or try again later.";
          }
          //regular logic for appropriate display messages
          if (response.success && response.success.totalcount && response.success.totalcount>0 && response.success.interactions && response.success.interactions.length) {
            $scope.adminappviewinteractsection.interactions = response.success.interactions;
            $scope.getnamesandunreadcount($scope.adminappviewinteractsection.interactions);
          } else {
            $scope.adminappviewinteractsection.nointeraction = true;
          }
          $scope.cs.showprogress = false;
          $scope.adminappviewinteractsection.interactionsloading = false;
      }, function (error) {
          $scope.adminappviewinteractsection.interactionsloadingerror = "Error: Could not load Interactions. Please try again later.";
          $scope.cs.showprogress = false;
          $scope.adminappviewinteractsection.interactionsloading = false;
      });
  }

  $scope.compose = function () {
    if ($scope.adminappviewinteractsection.selectedsectionid) {
      $state.go ("cs.interactions.newinteractionmessage",{sectionid: $scope.adminappviewinteractsection.selectedsectionid});
    }
  }

  $scope.openinteraction = function (interaction) {
    $state.go ("cs.interactions.interactionmessages.messageslist",{interactionid:interaction.id});
  }

}]);
