function addoreditfieldctrl($scope, $mdDialog, fieldset, editthisfieldindex, interactmoduleservice) {
  $scope.allowedfieldtypes = interactmoduleservice.allowedfieldtypes;
  $scope.editoraddlabel = 'Save';
  $scope.entryerror = null;

  $scope.newfield = {
    fieldname: '',
    fieldtype: '',
    fieldrequired: false,
    validvalues: []
  }

  //if editing, then initiate it to match the row being edited
  if (editthisfieldindex || editthisfieldindex==0) {
    $scope.editoraddlabel = 'Change';
    $scope.newfield = angular.copy(fieldset[editthisfieldindex]);
  } else {
    $scope.editoraddlabel = 'Add';
  }

  $scope.addoredit = function() {
    $scope.entryerror = null;
    $scope.entryerror = interactmoduleservice.validatethisentry($scope.newfield, fieldset, editthisfieldindex);
    if (!$scope.entryerror) {
      if (editthisfieldindex || editthisfieldindex==0) {
        fieldset[editthisfieldindex] = angular.copy($scope.newfield);
      } else {
        fieldset.push($scope.newfield);
      }
      $mdDialog.hide();
    }
  };
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
}
