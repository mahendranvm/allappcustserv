angular.module('csapp').controller('updateinteractmessageformctrl', ['$scope', '$state', 'Appuser', 'LoopBackAuth', '$mdMedia', '$mdDialog', 'interactmoduleservice', '$stateParams', function ($scope, $state, Appuser, LoopBackAuth, $mdMedia, $mdDialog, interactmoduleservice, $stateParams) {

  $scope.adminappupdateinteractmessageform = {}

  //this will be used to disable and enable the save button
  $scope.adminappupdateinteractmessageform.btndisabled = false;
  $scope.adminappupdateinteractmessageform.error = null;

  $scope.adminappupdateinteractmessageform.name = '';
  $scope.adminappupdateinteractmessageform.description ='';
  $scope.adminappupdateinteractmessageform.fields = angular.copy(interactmoduleservice.samplefields);

  $scope.adminappupdateinteractmessageform.selectedsectionid = null;
  $scope.adminappupdateinteractmessageform.selectedsection = null;
  $scope.adminappupdateinteractmessageform.firstmessageform = null;

  $scope.backtosection = function () {
    $state.go("cs.interactions.interactionslist",{sectionid: $stateParams.sectionid},{reload: true});
  }

  $scope.updatemessageformfn = function () {
    $scope.adminappupdateinteractmessageform.error = null;
    $scope.adminappupdateinteractmessageform.btndisabled = true;
    $scope.cs.showprogress = true;

    $scope.adminappupdateinteractmessageform.error = interactmoduleservice.validatethisfieldset($scope.adminappupdateinteractmessageform.fields);

    if (!$scope.adminappupdateinteractmessageform.error) {
      //update messageform info
      Appuser.prototype$a_attachMessageForm({ id: LoopBackAuth.currentUserId, realm: $scope.adminapp.selectedapp.id,
        sectionid: $scope.adminappupdateinteractmessageform.selectedsection.id, name: $scope.adminappupdateinteractmessageform.name, fields: $scope.adminappupdateinteractmessageform.fields
        },
        function (response) {
            $state.go("cs.interactions.interactionslist",{sectionid: $stateParams.sectionid},{reload: true});
            $scope.cs.showprogress = false;
            $scope.adminappupdateinteractmessageform.btndisabled = false;
        }, function (error) {
            if (error && error.data && error.data.error && error.data.error.details && error.data.error.details.appErrMsg) {
              $scope.adminappupdateinteractmessageform.error = error.data.error.details.appErrMsg;;
            }
            else {
              $scope.adminappupdateinteractmessageform.error = "Error: Could not update form. Please try again later.";
            }
            $scope.cs.showprogress = false;
            $scope.adminappupdateinteractmessageform.btndisabled = false;
        });
    } else {
      $scope.cs.showprogress = false;
      $scope.adminappupdateinteractmessageform.btndisabled = false;
    }
  };

  $scope.addfield = function(ev) {
    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
    $mdDialog.show({
      controller: addoreditfieldctrl,
      locals: {fieldset: $scope.adminappupdateinteractmessageform.fields,
              editthisfieldindex: null,
              interactmoduleservice: interactmoduleservice},
      templateUrl: 'app/_cs/_interact/_messageforms/addoreditfield.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: useFullScreen
    })
    .then(function() {
    }, function() {
    });
  }

  $scope.editfield = function(ev, index) {
    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
    $mdDialog.show({
      controller: addoreditfieldctrl,
      locals: {fieldset: $scope.adminappupdateinteractmessageform.fields,
              editthisfieldindex: index,
              interactmoduleservice: interactmoduleservice},
      templateUrl: 'app/_cs/_interact/_messageforms/addoreditfield.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: useFullScreen
    })
    .then(function() {
    }, function() {
    });
  }

  $scope.removefield = function (index) {
    $scope.adminappupdateinteractmessageform.fields.splice(index,1);
  }

  //if a section id was pre selected and passed as a state param, then use it
  if ($stateParams.sectionid) {
    $scope.adminappupdateinteractmessageform.selectedsectionid = $stateParams.sectionid;
    //when section details are resolved in parent abstract controller, determine the selected section here
    $scope.adminapp.selectedappPromise.$promise.then(function(){
      if (!$scope.adminappupdateinteractmessageform.selectedsection) {
        for (var i=0; i<$scope.adminapp.selectedapp.apprealmsections.length; i++) {
          if ($scope.adminapp.selectedapp.apprealmsections[i].id==$stateParams.sectionid) {
            $scope.adminappupdateinteractmessageform.selectedsection = $scope.adminapp.selectedapp.apprealmsections[i];
            $scope.adminappupdateinteractmessageform.firstmessageform = $scope.adminappupdateinteractmessageform.selectedsection.firstmessageform;
            $scope.adminappupdateinteractmessageform.name = $scope.adminappupdateinteractmessageform.firstmessageform.name;
            $scope.adminappupdateinteractmessageform.description = $scope.adminappupdateinteractmessageform.firstmessageform.description;
            $scope.adminappupdateinteractmessageform.fields = angular.copy($scope.adminappupdateinteractmessageform.firstmessageform.fields);
            break;
          }
        }
      }
    });
  }

}]);
