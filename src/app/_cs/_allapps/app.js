angular.module('csapp').controller('appctrl', ['$scope', '$state', '$stateParams', function ($scope, $state, $stateParams) {

  $scope.varallapps.currentnavitem = null;

  $scope.varallselectedapp = {};
  $scope.varallselectedapp.setcurrentrealmpromise = null;

  if ($stateParams.selectappid) {
    $scope.varallselectedapp.setcurrentrealmpromise = $scope.setcurrentrealm ($stateParams.selectappid);
  }

  $scope.goto = function (page) {
    if (page=='overview') $state.go ("cs.allapps.app.appoverview",{selectappid: $stateParams.selectappid})
    else if (page=='pushsettings') $state.go ("cs.allapps.app.pushsettings",{selectappid: $stateParams.selectappid})
  }

}]);
