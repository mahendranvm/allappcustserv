angular.module('csapp').controller('allappsctrl', ['$scope', '$state', '$stateParams', '$q', 'Appuser', 'LoopBackAuth', 'csconfig', function ($scope, $state, $stateParams, $q, Appuser, LoopBackAuth, csconfig) {

  csconfig.mode = "allapps";

  $scope.statuses = ['Not Published', 'Requested To Publish', 'Packaging To Publish', 'Submitted To App Stores',
                     'Partly Published', 'Published', 'Requested To Un-Publish', 'Un-Publish In Progress', 'Requested To Re-Publish']

  $scope.varallapps = {};
  $scope.varallapps.currentrealm = null;
  $scope.varallapps.apps = null;
  $scope.varallapps.noapp = false;
  $scope.varallapps.loading = true;
  $scope.varallapps.error = null;

  $scope.varallapps.appcount = null;
  $scope.varallapps.limit = 50;
  $scope.varallapps.skip = 0;

  $scope.varallapps.searchappid = null;
  $scope.varallapps.searchappstatus = null;
  $scope.varallapps.searchappname = null;

  $scope.varallapps.currentnavitem = null;

  $scope.searchappload = function () {
    $scope.cs.showprogress = true;
    var inputparams = { id: LoopBackAuth.currentUserId, limit: $scope.varallapps.limit, skip: $scope.varallapps.skip};

    if ($scope.varallapps.searchappid) inputparams.appid = $scope.varallapps.searchappid;
    if ($scope.varallapps.searchappname) inputparams.appname = $scope.varallapps.searchappname;
    if ($scope.varallapps.searchappstatus && $scope.varallapps.searchappstatus.length) inputparams.appstatus = $scope.varallapps.searchappstatus;

    Appuser.prototype$c_getAllApps( inputparams ,
      function (response) {
          if (response.success && response.success.apps && response.success.apps.length) {
            $scope.varallapps.apps = response.success.apps;
            $scope.varallapps.appcount = response.success.appcount;
            $scope.varallapps.noapp = false;
          } else {
            $scope.varallapps.noapp = true;
          }
          $scope.cs.showprogress = false;
          $scope.varallapps.loading = false;
      }, function (error) {
          $scope.varallapps.error = "Error getting apps. Try again later.";
          $scope.cs.showprogress = false;
          $scope.varallapps.loading = false;
      });
  }

  $scope.next = function () {
    $scope.varallapps.skip = $scope.varallapps.skip + $scope.varallapps.limit;
    $scope.searchappload();
  }

  $scope.previous = function () {
    $scope.varallapps.skip = $scope.varallapps.skip - $scope.varallapps.limit;
    $scope.searchappload();
  }

  $scope.select = function (app) {
    $state.go ("cs.allapps.app.appoverview",{selectappid: app.id})
  }

  $scope.setcurrentrealm = function (appid) {
    // perform some asynchronous operation, resolve or reject the promise when appropriate.
    return $q(function(resolve, reject) {
        var matchfound = false;
        if ($scope.varallapps.apps && $scope.varallapps.apps.length) {
          for (var i=0; i<$scope.varallapps.apps.length; i++) {
            if ($scope.varallapps.apps[i].id==appid) {
              matchfound = true;
              $scope.varallapps.currentrealm = $scope.varallapps.apps[i];
              break;
            }
          }
        }
        if (!matchfound) {
          Appuser.prototype$c_getAllApps( {id: LoopBackAuth.currentUserId, appid: appid} ,
            function (response) {
                if (response.success && response.success.apps && response.success.apps.length) {
                  $scope.varallapps.currentrealm = response.success.apps[0];
                } else {
                  $scope.varallapps.currentrealm = null;
                }
                $scope.cs.showprogress = false;
                resolve ();
            }, function (error) {
                $scope.varallapps.currentrealm = null;
                $scope.cs.showprogress = false;
                resolve ();
            });
        } else {
          resolve ();
        };
      });
    }

  if ($stateParams.searchskip) $scope.varallapps.skip = $stateParams.searchskip;
  if ($stateParams.searchappid) $scope.varallapps.searchappid = $stateParams.searchappid;
  if ($stateParams.searchappnam) $scope.varallapps.searchappname = $stateParams.searchappname;
  if ($stateParams.searchappstatus) $scope.varallapps.searchappstatus = $stateParams.searchappstatus;
  $scope.searchappload();

}]);
