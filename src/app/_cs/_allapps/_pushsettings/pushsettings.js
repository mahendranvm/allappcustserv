angular.module('csapp').controller('pushsettingsctrl', ['$scope', '$state', '$stateParams', 'Appuser', 'LoopBackAuth', 'csconfig', function ($scope, $state, $stateParams, Appuser, LoopBackAuth, csconfig) {

  $scope.varallapps.currentnavitem = "pushsettings";

  $scope.varupdateapppush = {};
  $scope.varupdateapppush.apnsbundleid = null;
  $scope.varupdateapppush.gcmsenderapikey = null;

  //this will be used to disable and enable the save button
  $scope.varupdateapppush.btndisabled = false;
  $scope.varupdateapppush.error = null;

  $scope.varallselectedapp.setcurrentrealmpromise.then(function(){
    $scope.varupdateapppush.apnsbundleid = $scope.varallapps.currentrealm.apnsbundleid;
    $scope.varupdateapppush.gcmsenderapikey = $scope.varallapps.currentrealm.gcmsenderapikey;
  })

  $scope.updateapppushfn = function () {
    $scope.varupdateapppush.error = null;
    $scope.varupdateapppush.btndisabled = true;
    $scope.cs.showprogress = true
    Appuser.prototype$c_updatePushSettings({ id: LoopBackAuth.currentUserId, realm: $scope.varallapps.currentrealm.id,
      apnsbundleid:$scope.varupdateapppush.apnsbundleid, gcmsenderapikey: $scope.varupdateapppush.gcmsenderapikey},
        function (response) {
            $state.go("cs.allapps.app.appoverview",{searchappid: $scope.varallapps.searchappid, searchappname: $scope.varallapps.searchappname, searchappstatus: $scope.varallapps.searchappstatus, searchskip: $scope.varallapps.skip},{reload:true});
            $scope. cs.showprogress = false;
            $scope.varupdateapppush.btndisabled = false;
        }, function (error) {
            $scope.varupdateapppush.error = "Error: Could not Update. Please try again later.";
            $scope.cs.showprogress = false;
            $scope.varupdateapppush.btndisabled = false;
        });
  };
}]);
