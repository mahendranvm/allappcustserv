angular.module('csapp').controller('appoverviewctrl', ['$scope', '$state', '$stateParams', 'Appuser', 'LoopBackAuth', '$mdDialog', function ($scope, $state, $stateParams, Appuser, LoopBackAuth, $mdDialog) {

  $scope.varallapps.currentnavitem = "overview";

  $scope.varallselectedapp.logs = [];
  $scope.varallselectedapp.logsloading = true;
  $scope.varallselectedapp.logserror = null;
  $scope.varallselectedapp.nologs = false;

  $scope.varallselectedapp.updateerror = null;

    Appuser.prototype$c_getAppLogs ( { id: LoopBackAuth.currentUserId, realm: $stateParams.selectappid} ,
      function (response) {
          if (response.success && response.success.length) {
            $scope.varallselectedapp.logs = response.success;
            $scope.varallselectedapp.nologs = false;
          } else {
            $scope.varallselectedapp.nologs = true;
          }
          $scope.varallselectedapp.logsloading = false;
          $scope.varallselectedapp.logserror = null;
      }, function (error) {
        $scope.varallselectedapp.logsloading = false;
        $scope.varallselectedapp.logserror = "Error Loading Logs.";
      });


    $scope.changestatusto = function (app, newstatus, ev) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.prompt()
        .title('Are you sure you want to change status to ' + newstatus + '?')
        .textContent('If Yes, please enter a comment explaining the reason for change.')
        .placeholder('Change Reason')
        .targetEvent(ev)
        .required(true)
        .ok('Change')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function(changereason) {
        $scope.cs.showprogress = true;
        Appuser.prototype$c_updateAppStatus({ id: LoopBackAuth.currentUserId, realm: app.id, status: newstatus, changereason: changereason},
          function (response) {
              $scope.cs.showprogress = false;
              $state.go("cs.allapps.app.appoverview",{searchappid: $scope.varallapps.searchappid, searchappname: $scope.varallapps.searchappname, searchappstatus: $scope.varallapps.searchappstatus, searchskip: $scope.varallapps.skip},{reload:true});
          }, function (error) {
              $scope.varallselectedapp.updateerror = "Error updating. Try again later.";
              $scope.cs.showprogress = false;
          });
      }, function() {

      });
    }

    $scope.changesubscriptionperiod = function (app, subscriptiontype, ev) {
      $mdDialog.show({
         controller: DialogController,
         template: '<md-dialog class="md-padding">' +
            '<md-dialog-content class="md-padding">' +
              '<h4>Enter Current Period End Date</h4>'+
              '<md-datepicker ng-model="publishDate"></md-datepicker>' +
            '</md-dialog-content>' +
            '<md-dialog-actions>' +
              '<md-button ng-click="cancel()" class="md-raised">Cancel</md-button>' +
              '<md-button ng-click="hide()" class="md-raised md-primary">Ok</md-button>' +
            '</md-dialog-actions>' +
          '</md-dialog>',
         parent: angular.element(document.body),
         targetEvent: ev,
         clickOutsideToClose:true
       })
       .then(function(publishDate) {
         $scope.cs.showprogress = true;
         Appuser.prototype$c_updateSubscriptionCycle({ id: LoopBackAuth.currentUserId, realm: app.id, trialenddate: publishDate, subscriptiontype: subscriptiontype},
           function (response) {
               $scope.cs.showprogress = false;
               $state.go("cs.allapps.app.appoverview",{searchappid: $scope.varallapps.searchappid, searchappname: $scope.varallapps.searchappname, searchappstatus: $scope.varallapps.searchappstatus, searchskip: $scope.varallapps.skip},{reload:true});
           }, function (error) {
               $scope.varallselectedapp.updateerror = "Error updating. Try again later.";
               $scope.cs.showprogress = false;
           });
       }, function() {

       });

    }

    function DialogController($scope, $mdDialog) {
        $scope.publishDate = new Date();

        $scope.hide = function() {
          $mdDialog.hide($scope.publishDate);
        };

        $scope.cancel = function() {
          $mdDialog.cancel();
        };

      }

}]);
