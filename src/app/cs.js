angular.module('csapp').controller('csctrl', ['$scope', '$state', 'csconfig', 'Appuser', 'LoopBackAuth', function ($scope, $state,csconfig, Appuser, LoopBackAuth) {

  $scope.csconfig = csconfig;
  $scope.cs = {};
  $scope.cs.showprogress = true;

  $scope.adminapp = {};
  $scope.adminapp.apploading = true;

  $scope.adminapp.selectedappPromise = Appuser.prototype$c_getAdminAppInfo({ id: LoopBackAuth.currentUserId},
          function (response) {
              if (response.success) {
                $scope.adminapp.selectedapp = response.success; //get the selected app info into scope
                $scope.adminapp.noapp = false; //app is available, set noapp flag to false
              } else {
                $scope.adminapp.noapp = true;
              }
              $scope.adminapp.apploading = false;
              $scope.cs.showprogress = false;
          }, function (error) {
              $scope.adminapp.apperror = error;
              $scope.adminapp.apploading = false;
              $scope.cs.showprogress = false;
          });

}]);
